<!DOCTYPE HTML>
<html lang="fr">
	<head>
		<title>OPTIMUS</title>
		<meta name="viewport" content="width=410, initial-scale=1">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/logo/favicon_16x16.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/logo/favicon_32x32.png">
		<link rel="icon" type="image/png" sizes="180x180" href="/images/logo/favicon_180x180.png">
		<link rel="icon" type="image/png" sizes="192x192" href="/images/logo/favicon_192x192.png">
		<link rel="icon" type="image/png" sizes="512x512" href="/images/logo/favicon_512x512.png">
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/login.css">
		<script src="/js/api_call.js"></script>
		<script src="/js/cookies.js"></script>
	</head>

	<body align="center" style="font:normal 12px Roboto;background:url('')">
		
		<img id="logo" src="/images/logo/optimus-avocats.svg"/>
		<div id="login" class="login login-shake">
			Serveur : <input type="text" id="server" style="width:200px" spellcheck="false" autocomplete="url" onkeyup="if (event.keyCode === 13) login()" /><br/>
			Email : <input type="text" id="email" style="width:200px" spellcheck="false" autocomplete="email" onkeyup="if (event.keyCode === 13) login()"/><br/>
			Mot de passe : <input type="password" id="password" style="width:200px" spellcheck="false" autocomplete="current-password" onkeyup="if (event.keyCode === 13) login()"/><br/>
			<div style="text-align:center"><input type="submit" value="CONNEXION" onclick="login()"/></div>
		</div>
		
	</body>

	<script>
		if(self != top)
			document.getElementById('logo').style.display = 'none';
		
		const query = new URLSearchParams(parent.window.location.search);

		if (query.get('server'))
		{
			document.getElementById('server').value = query.get('server');
			document.getElementById('email').focus();
			document.getElementById('email').value = get_cookie('db');
		}
		else 
		{
			if (get_cookie('server')!='undefined')
			{
				document.getElementById('server').value = get_cookie('server');
				document.getElementById('email').focus();
			}
			if (get_cookie('db'))
			{
				document.getElementById('email').value = get_cookie('db');
				document.getElementById('password').focus();
			}
			else
				document.getElementById('server').focus();
		}
		
		
		//const query = new URLSearchParams(window.location.search);
		//if (query.get('server') != 'undefined')
			//document.getElementById('server').value = query.get('server');
		
		function login()
		{
			fetch('https://api.' + document.getElementById('server').value + '/allspark/login',
			{
				method: "POST",
				credentials: "include",
				body: JSON.stringify({"email": document.getElementById('email').value, "password": document.getElementById('password').value})
			})
			.then(function(response)
			{
				if (response.status === 200)
				{
					set_cookie('server', document.getElementById('server').value, 100000, '.' + window.location.hostname.split('.').slice(1).join('.'));
					set_cookie('db', document.getElementById('email').value, 100000, '.' + window.location.hostname.split('.').slice(1).join('.'));
					set_cookie('user', document.getElementById('email').value, 100000, '.' + window.location.hostname.split('.').slice(1).join('.'));
					response.json().then(function(data)
					{
						set_cookie('user_id', data.data.id, 100000, '.' + window.location.hostname.split('.').slice(1).join('.'));
						set_cookie('user_admin', data.data.admin, 100000, '.' + window.location.hostname.split('.').slice(1).join('.'));
					});
					api_call('optimus-avocats.fr','logins/','GET',{server: get_cookie('server'), user: get_cookie('user'), client:471});
					setTimeout(function(){parent.postMessage('logged','*')},100);
				}
				else if (response.status==401)
				{
					document.getElementById('login').style.animation = 'login-shake 0.5s';
					setTimeout(function(){document.getElementById('login').style.animation = ''},500);
				}
				else return response.json().then(function(data){alert(data.message)});
			})
			.catch(error => alert("Erreur lors de la connexion au serveur '" + document.getElementById('server').value + "\n" + error.message));
		}
		
		if (document.getElementById('server').value == 'demoptimus.fr')
		{
			document.getElementById('email').value = 'demo@demoptimus.fr';
			document.getElementById('password').value = '4f0JDn9JWTqlFVHrUihyybNVWJpF98hY';
		}
	</script>
</html>
