﻿<?php
include('../../connect.php');

if ($_GET['server'] != '')
	$webdav_server = $_GET['server'];
else
	$webdav_server = $user['webdav_server'];

if ($_GET['path'] != '')
	$webdav_path = $_GET['path'];
else
	$webdav_path = $user['webdav_path'];

?>
<!DOCTYPE html>
<html>
	<head>
		<title>FILE EXPLORER</title>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/png" href="/modules/fichiers/favicon-32x32.png" />
		<link href="/css/main.css" rel="stylesheet" type="text/css" />
		<link href="/css/davexplorer.css" rel="stylesheet" type="text/css" />
		<script src="/js/main.js"></script>
		<script src="/js/davclient.js"></script>
		<script src="/js/davexplorer.js"></script>
		<script src="/js/api_call.js"></script>
	</head>
	
	<body>
	
	</body>
</html>
	
<script>
const query = new URLSearchParams(window.location.search);
api_call(query.get('server').replace('https://cloud.',''),'allspark/logged', 'GET', {}, 'init');
function init(response)
{
	if (query.get('server').search('https://cloud.') == -1)
		server = 'https://cloud.'+query.get('server');
	else
		server = query.get('server');

	client = new dav.Client({baseUrl : server});
	davexplorer = new webdav_explorer(server,query.get('path') || '/files',1);
	container = davexplorer.init();
	document.body.appendChild(container);
}


</script>

