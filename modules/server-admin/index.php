<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta name="viewport" content="width=410, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="/modules/server-admin/icon.svg" />
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/tabs.css">
		<script src="/js/main.js"></script>
	</head>

	<body style="text-align:center;overflow-y:hidden" onload="load_tab()">
		<div class="tabs" onclick="if (event.target.name=='tabs') document.getElementById('0').checked = false">
			<input id="0" type="checkbox"/><label for="0"></label>
			<input id="1" type="radio" name="tabs" onclick="load_tab()" link="tab_general.php" checked/><label for="1">ADMINISTRATION SERVEUR</label>
		</div>
		<div class="tab_separator"></div>
		<iframe id="frame" marginwidth="0" marginheight="0" frameborder="0" onload="this.style.height = (window.innerHeight - 57) + 'px'" style="width:100%"></iframe>
	</body>
</html>