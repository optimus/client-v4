<!DOCTYPE html>
<html lang="fr">

<head>
	<title>SERVER-ADMIN</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/server-admin/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/editor.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div class="flex_container" style="max-width:926px;margin:auto">

		<div class="editor" style="min-width:410px">
			<div id="api_title">API</div>
			<div style="text-align:center;line-height:18px">
				Origines autorisées par les API :<br/>
				<table id="api"></table>
				<button onclick="add_origin({})" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter une origine</button>
			</div>
		</div>

		<div class="editor" style="min-width:410px">
			<div id="mailserver-domains_title">SERVEUR MAIL</div>
			<div style="text-align:center;line-height:18px">
				Domaines gérés par le serveur mail :<br/>
				<table id="mailserver-domains"></table>
				<button onclick="add_domain({})" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un domaine</button>
			</div>
		</div>

		<div class="editor" style="min-width:410px">
			<div>MISES A JOUR</div>
			<div style="text-align:center;line-height:16px;color:#FFFFFF">
				<table class="statictable">
					<thead>
						<tr>
							<td style="padding:10px">API</td>
							<td style="padding:10px">VERSION<br/>ACTUELLE</td>
							<td style="padding:10px">VERSION<br/>DISPONIBLE</td>
							<td style="padding:10px">MAJ</td>
						</tr>
					</thead>
					<tbody id="updates"></tbody>
				</table>
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'ADMIN';
		
		if (get_cookie('user_admin')==1)
			api_call(query.get('server'),'allspark/logged','GET', {},'init');
		else
			alert('ce module est réservé aux administrateurs');
		
		function init(response)
		{
			services = api_call_sync(query.get('server'),'allspark/services','GET', {});
			if (services.data)
				for (service of services.data)
					api_call(query.get('server'),service.name + '/service','GET', {},'add_service');
			
			origins = api_call_sync(query.get('server'),'allspark/allowed-origins','GET', {});
			if (origins.data)
				for(origin of origins.data)
					add_origin(origin);
			
			domains = api_call_sync(query.get('server'),'allspark/mailserver-domains','GET', {});
			if (domains.data)
				for(domain of domains.data)
					add_domain(domain);
		}

		function add_service(service)
		{
			tr = document.getElementById('updates').insertRow();
			tr.id = service.data.name;
			td0 = tr.insertCell();
			td0.innerHTML = service.data.name.toUpperCase();
			td1 = tr.insertCell();
			td1.innerHTML = service.data.current_version + '<br/>' + service.data.current_date;
			td2 = tr.insertCell();
			td2.innerHTML = service.data.latest_version + '<br/>' + service.data.latest_date;
			td3 = tr.insertCell();
			if (service.data.latest_date > service.data.current_date)
			{
				img = document.createElement('img');
				img.src =  '/lib/fontawesome/sync.svg';
				img.style = 'height:16px;opacity:0.6;vertical-align:-4px';
				td3.style.cursor = 'pointer';
				td3.onclick = function()
				{
					this.firstChild.style.animation = 'rotation 1s infinite linear';
					api_call(query.get('server'), service.data.name+'/service','POST', {}, 'updated_service', this);
				}
				td3.appendChild(img);
			}
		}

		function updated_service(service, obj)
		{
			if (service.code == 201)
			{
				localStorage.clear();
				window.location.reload();
			}
			else
				alert(service.message);
			obj.firstChild.style.animation = '';
		}

		function add_origin(origin)
		{
			tr = document.getElementById('api').insertRow();
			td = tr.insertCell();
			td.id = origin.id;
			td.style.textAlign = 'center';
			td.innerHTML = 'https://';
			input = document.createElement('input');
			input.style.width = '306px';
			input.type = 'text';
			input.value = origin.origin || null;
			input.lastvalue = origin.origin || null;
			input.onchange = function()
			{
				this.value = this.value.toLowerCase();
				if (this.value == this.lastvalue)
					return false;
				if (this.parentNode.id == 'undefined')
				{
					result = api_call_sync(query.get('server'),'allspark/allowed-origins','POST', {'origin':this.value});
					this.parentNode.id = result.data.id;
				}
				else
				{
					if ((this.lastvalue == window.location.hostname || this.lastvalue == '*'+window.location.hostname.substring(window.location.hostname.indexOf('.'))) && !confirm("Cette origine semble être celle que vous utilisez actuellement.\nLa modifier risque d'empecher toute interaction avec ce client.\nDans ce cas, vous ne pourrez plus ajouter d'origine ou restaurer le réglage\nEtes vous sûr ?"))
						return false;
					result = api_call_sync(query.get('server'),'allspark/allowed-origins/'+this.parentNode.id,'PATCH', {'origin':this.value});
					if (result.code != 200)
					{
						this.focus();
						return false;
					}
				}
				this.lastvalue = this.value;
				this.classList.remove('editing');
			}
			input.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			input.onfocus = function(){this.classList.add('editing')};
			input.onblur = function(){if (this.lastvalue == this.value) this.classList.remove('editing'); else this.focus()}
			td.appendChild(input);
			if (origin.origin == null)
				input.focus();
			deleter = document.createElement('img');
			deleter.src = '/lib/fontawesome/trash.svg';
			deleter.style = 'height:18px;vertical-align:-4px;margin-left:4px;filter:contrast(60%);cursor:pointer';
			deleter.onclick = function()
			{
				if (!this.parentNode.id)
					this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				else
				{
					if ((this.parentNode.getElementsByTagName('input')[0].value == window.location.hostname || this.parentNode.getElementsByTagName('input')[0].value == '*'+window.location.hostname.substring(window.location.hostname.indexOf('.'))) && !confirm("Cette origine semble être celle que vous utilisez actuellement.\nLa supprimer risque d'empecher toute interaction avec ce client.\nDans ce cas, vous ne pourrez plus ajouter d'origine ou restaurer le réglage\nEtes vous sûr ?"))
						return false;
					result = api_call_sync(query.get('server'),'allspark/allowed-origins/'+this.parentNode.id,'DELETE', {});
					if (result.code == 200)
						this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				}
			};
			td.appendChild(deleter);
		}

		function add_domain(domain)
		{
			tr = document.getElementById('mailserver-domains').insertRow();
			td = tr.insertCell();
			td.id = domain.id;
			td.style.textAlign = 'center';
			input = document.createElement('input');
			input.style.width = '348px';
			input.type = 'text';
			input.value = domain.domain || null;
			input.lastvalue = domain.domain || null;
			input.onchange = function()
			{
				this.value = this.value.toLowerCase();
				if (this.value == this.lastvalue)
					return false;
				if (this.parentNode.id == 'undefined')
				{
					result = api_call_sync(query.get('server'),'allspark/mailserver-domains','POST', {'domain':this.value});
					this.parentNode.id = result.data.id;
				}
				else
				{
					result = api_call_sync(query.get('server'),'allspark/mailserver-domains/'+this.parentNode.id,'PATCH', {'domain':this.value});
					if (result.code != 200)
					{
						this.focus();
						return false;
					}
				}
				this.lastvalue = this.value;
				this.classList.remove('editing');
			}
			input.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			input.onfocus = function(){this.classList.add('editing')};
			input.onblur = function(){if (this.lastvalue == this.value) this.classList.remove('editing'); else this.focus()}
			td.appendChild(input);
			if (domain.domain == null)
				input.focus();
			deleter = document.createElement('img');
			deleter.src = '/lib/fontawesome/trash.svg';
			deleter.style = 'height:18px;vertical-align:-4px;margin-left:4px;filter:contrast(60%);cursor:pointer';
			deleter.onclick = function()
			{
				if (!this.parentNode.id)
					this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				else
				{
					result = api_call_sync(query.get('server'),'allspark/mailserver-domains/'+this.parentNode.id,'DELETE', {});
					if (result.code == 200)
						this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				}
			};
			td.appendChild(deleter);
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>