<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>IMPORTS CSV</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/davclient.js"></script>
</head>
	

<body>

	<br/><br/>

	<button onclick="import_all()">IMPORTER TOUT</button>

	<br/><br/><br/>
	
	<table id="folders" class="statictable">
		<thead>
			<tr>
				<td>DOSSIER</td>
				<td>NUMERO</td>
				<td>NOM</td>
				<td>OUVERTURE</td>
				<td>IMPORT</td>
				<td>ERRORS</td>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<script type="text/javascript">

	
	
	const query = new URLSearchParams(window.location.search);
		
	api_call(query.get('server'),'allspark/logged', 'GET', {}, 'init');
	
	function init(user)
	{
		client = new dav.Client({baseUrl : 'https://cloud.' + query.get('server')});
		client.propFind('/files/' + user.data.email + '/==IMPORTS==', ['{DAV:}getcontentlength', '{DAV:}getlastmodified', '{DAV:}resourcetype'], '1').
		then((response) =>
		{
			response.body = response.body.slice(1)
			response.body.sort((a,b) => a.href.localeCompare(b.href))
			console.log(response)
			for (let folder of response.body)
			{
				folder.path = folder.href
				folder.parentfolder = folder.href.split('/').slice(0,-2).join('/')
				folder.user = user.data.email
				folder.dossier = decodeURIComponent(folder.href).split('/')[4]
				folder.numero = decodeURIComponent(folder.href).split('/')[4].split(' - ')[0].substr(0,2) + '/' + decodeURIComponent(folder.href).split('/')[4].split(' - ')[0].substr(2)
				folder.nom = decodeURIComponent(folder.href).split('/')[4].substr(9).replace('&','ET')
				folder.ouverture = '20' + decodeURIComponent(folder.href).split('/')[4].substr(0,2) + '-01-01'

				doublon = false
				for (let row of document.getElementById('folders').tBodies[0].rows)
					if (row.cells[2].innerText == folder.nom)
					{
						doublon = true
						folder.nom =  isNaN(folder.nom.split(' ').pop()) ? folder.nom + ' 2' : folder.nom.split(' ').slice(0,-1).join(' ') + ' ' + (parseInt(folder.nom.split(' ').pop()) + 1)
					}
						

				tr = document.getElementById('folders').tBodies[0].insertRow();
				tr.data = folder

				td = tr.insertCell();
				td.style.textAlign = 'left'
				td.innerText = folder.dossier

				td = tr.insertCell();
				td.style.textAlign = 'center'
				td.innerText = folder.numero

				td = tr.insertCell();
				td.style.textAlign = 'left'
				td.innerText = folder.nom

				td = tr.insertCell();
				td.style.textAlign = 'center'
				td.innerText = folder.ouverture

				td = tr.insertCell();
				td.innerHTML = '<button>Importer</button>'
				td.onclick = function() {import_folder(this.parentNode, folder)}

				td = tr.insertCell();
				td.style.textAlign = 'center'
				if (doublon)
					td.innerText = 'DOUBLON : Le dossier a été renommé'
				
			}
			
		})
	}

	function import_folder(row, folder)
	{
		imported = api_call_sync(query.get('server'),'optimus-avocats/1/dossiers', 'POST', {"numero": folder.numero, "nom": folder.nom, "ouverture": folder.ouverture})
		if (imported.code == 201 || imported.code == 409)
		{
			request = client.request('MOVE', folder.path, { 'Destination': 'https://cloud.' + query.get('server') + '/files/' + encodeURI(folder.user) + '/' + encodeURI('==DOSSIERS==') + '/' + encodeURI(folder.nom) })
			.then(function (result) 
			{
				if (result.status < 300)
					row.remove();
				else
				{
					row.style.background = '#FF0000';
					row.cells[5].innerText = 'ERROR ' + result.status + ' : ' + result.xhr.statusText + "\n" + result.xhr.responseText.split('<s:message>').pop().split('</s:message>').shift()
				}
			})
		}
		else
		{
			row.style.background = '#FF0000';
			row.cells[5].innerText = 'ERROR ' + imported.code + ' : ' + imported.message;
		}
			

	}
	
	function import_all()
	{
		for (let folders of document.getElementById('folders').tBodies[0].rows)
			folders.cells[4].click();
	}

	</script>
</body>
</html>