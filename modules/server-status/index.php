<!doctype html>
<html>
	<head>
		<title>SERVER-STATUS</title>
		<link rel="shortcut icon" type="image/png" href="/modules/server-status/favicon-32x32.png">
		<link href="/css/main.css" rel="stylesheet" type="text/css">
		<link href="/css/editor.css" rel="stylesheet" type="text/css">
		<link href="/css/statictable.css" rel="stylesheet" type="text/css">
		<script src="/js/api_call.js"></script>
		<script src="/js/main.js"></script>
		<script src="/js/editor.js"></script>
		<script src="/js/number_format.js"></script>
		<script src="/js/cookies.js"></script>

		<style>
			meter
			{
				position: relative;
				margin-top:60px;
				width: 500px;
				height: 20px;
				padding:2px;
			}
			
			meter::-webkit-meter-bar
			{
				background: none;
				background-color: silver;
				height:20px;
			}

			meter::-webkit-meter-optimum-value
			{
				background: none;
				background-color: #00BB00;
				height:20px;
			}

			.sysdisk::before
			{
				content: attr(hdd-system-before);
				position: absolute;
				top: -100%;
				left: 0
			}

			.sysdisk::after
			{
				content: attr(hdd-system-after);
				position: absolute;
				top: 140%;
				left: 0
			}

			.datadisk::before
			{
				content: attr(hdd-data-before);
				position: absolute;
				top: -100%;
				left: 0
			}

			.datadisk::after
			{
				content: attr(hdd-data-after);
				position: absolute;
				top: 120%;
				left: 0
			}
		</style>
	</head>

	<body>

		<div style="font-size:32px;margin:30px">ETAT DU SERVEUR</div>

		<table id="server-status" class="statictable" align="center" style="border-collapse:collapse;margin-top:20px">
			<thead>
				<tr>
					<td align="center">STATUS</td>
					<td align="center">APPLICATION</td>
					<td align="center">MODULE</td>
					<td align="center">VERSION</td>
					<td align="center">NOTES</td>
				</tr>
			</thead>
			<tbody id="services"/>
		</table>

		<br/>
		<br/>

		PROCESSEUR : <span id="cpu-usage"></span><br/>
		<br/>
		MEMOIRE : <span id="mem-usage"></span><br/>

		<meter id="disk-system-usage" class="sysdisk" max="100"></meter><br/>
		<br/>
		<meter id="disk-data-usage" class="datadisk" max="100"></meter><br/>
		<br/>
		<br/>
		<br/>
		Espace libre non partitionné : <span id="freespace"></span>

	</body>

	<script language="JavaScript" type="text/javascript">	
		const query = new URLSearchParams(window.location.search);
		module = 'server-status';

		api_call(query.get('server'),'allspark/status','GET',{},'init');
		function init(status)
		{
			for (services of status.data.services)
			{
				tr = document.getElementById('services').insertRow();
				td1 = tr.insertCell();
				if (services.status=='active')
					td1.innerHTML = '<div style="margin:auto;width:12px;height:12px;background:#00FF00"/>';
				else
					td1.innerHTML = '<div style="margin:auto;width:12px;height:12px;background:#FF0000"/>';
				td2 = tr.insertCell();
				td2.style.textAlign = 'left';
				td2.innerHTML = services.type;
				td3 = tr.insertCell();
				td3.style.textAlign = 'left';
				td3.innerHTML = services.name;
				td4 = tr.insertCell();
				td4.style.textAlign = 'right';
				td4.innerHTML = services.version;
				td5 = tr.insertCell();
				td5.innerHTML = services.other || null;
			}
			
			document.getElementById('cpu-usage').innerHTML = status.data.cpu.usage.toFixed(0) + '%';
			document.getElementById('mem-usage').innerHTML = status.data.memory.usage.toFixed(0) + '%';
			
			document.getElementById('disk-system-usage').value = parseInt(status.data.disk[0].usage);
			document.getElementById('disk-system-usage').innerHTML = status.data.disk[0].usage;
			document.getElementById('disk-system-usage').setAttribute('hdd-system-before', status.data.disk[0].name + " (" + status.data.disk[0].device + ")");
			document.getElementById('disk-system-usage').setAttribute('hdd-system-after', (status.data.disk[0].used/1024/1024).toFixed(2) + " Go / " + (status.data.disk[0].total/1024/1024).toFixed(2) + " Go");
			document.getElementById('disk-data-usage').value = parseInt(status.data.disk[1].usage);
			document.getElementById('disk-data-usage').innerHTML = status.data.disk[1].usage;
			document.getElementById('disk-data-usage').setAttribute('hdd-data-before', status.data.disk[1].name + " (" + status.data.disk[1].device + ")");
			document.getElementById('disk-data-usage').setAttribute('hdd-data-after', (status.data.disk[1].used/1024/1024).toFixed(2) + " Go / " + (status.data.disk[1].total/1024/1024).toFixed(2) + " Go");
		
			document.getElementById('freespace').innerHTML = Math.round(status.data.freespace/1024/1024) + ' Go';
		}
	</script>
</html>