<!doctype html>
<html>
	<head>
		<title>SERVER-REBOOT</title>
		<link rel="shortcut icon" type="image/png" href="/modules/server-reboot/favicon-32x32.png">
		<link href="/css/main.css" rel="stylesheet" type="text/css">
		<script src="/js/api_call.js"></script>
		<script src="/js/cookies.js"></script>
	</head>

	<body>

		<div style="font-size:32px;margin:30px">REDEMARRAGE EN COURS<br/>Veuillez patienter</div>

		<img id="waiter" src="/images/waiter.gif"/>

	</body>

	<script language="JavaScript" type="text/javascript">	
		const query = new URLSearchParams(window.location.search);
		module = 'server-reboot';

		api_call(query.get('server'),'allspark/reboot','GET',{},'init');

		waiter = setInterval(function()
		{
			result = api_call_sync(query.get('server'),'allspark/ping','GET',{});
			if (result.code = 200)
			{
				clearInterval(waiter);
				document.body.removeChild(document.getElementById('waiter'));
				window.close();
				return true;
			}
				
		},500);
		
	</script>
</html>