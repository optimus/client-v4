<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
		<script src="/js/editor.js"></script>
	</head>
	
	<body>

		<textarea id="notes" onblur="simple_save(this)" style="position:fixed;top:50%;left:50%;width:90%;height:90%;transform: translate(-50%, -50%);"></textarea>
		
		<script type="text/javascript">
			const query = new URLSearchParams(window.location.search);
			api = 'optimus-avocats'
			resource = 'contacts';
			itm = {};
			authorizations = {};
			
			api_call(query.get('server'), 'optimus-avocats/'+query.get('owner')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				editor_init();
				document.body.style.display = 'inline';
			}
		</script>
	</body>
</html>