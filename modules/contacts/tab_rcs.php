<!DOCTYPE html>
<html lang="fr" style="height:100%">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<script type="text/javascript" src="/js/main.js"></script>
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript">
			
			const query = new URLSearchParams(window.location.search);
			api_call(query.get('server'), 'optimus-avocats/'+query.get('owner')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				if (response.data[0].siret)
				{
					window.location.href = 'https://www.societe.ninja/data.php?siren=' + response.data[0].siret;
					parent.document.getElementById("frame").height = parent.document.getElementById("frame").contentWindow.document.body.scrollHeight + "px";
				}
				else
				{
					window.location.href = 'https://www.societe.ninja';
					notification("Merci de renseigner le n° SIRET de l'entreprise pour activer ce module","#FF0000");
				}
			}
		</script>
	</head>
</html>