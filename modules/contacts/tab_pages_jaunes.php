<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="stylesheet" href="/css/statictable.css" />
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript">
			const query = new URLSearchParams(window.location.search);
			api_call(query.get('server'), 'optimus-avocats/'+query.get('owner')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				if (response.data[0].type == 30)
					window.open('https://www.pagesjaunes.fr/recherche/' + response.data[0].city_name + "-" + response.data[0].zipcode.substring(0,2) + "/" + response.data[0].company_name.toLowerCase().replace(' ','-'));
				else
					window.open('https://www.pagesjaunes.fr/pagesblanches/recherche?quoiqui=' + response.data[0].company_name.toLowerCase() + "%20" + response.data[0].firstname + "%20" + response.data[0].lastname + "&ou=" + response.data[0].city_name + '&proximite=0');
			}
		</script>
	</head>
</html>