<!DOCTYPE html>
<html lang="fr" style="height:100%">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<script type="text/javascript" src="/js/main.js"></script>
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbmr-EKcnrIdz6xWEORTTnoauDz36jbew&sensor=true"></script>
		<script type="text/javascript">
			
			const query = new URLSearchParams(window.location.search);
			itm = {};
			authorizations = {};
			huissiers = {};
			
			api_call(query.get('server'), 'optimus-avocats/'+query.get('owner')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				
				huissiers = api_call_sync('optimus-avocats.fr', 'huissiers/', 'GET', {"commune_insee":response.data[0].city});

				var latlon = new XMLHttpRequest();
				latlon.open('GET', 'https://nominatim.openstreetmap.org/search?format=json&limit=3&q=' + itm.zipcode +', '+ itm.city_name, false);
				latlon.send();
				lat = JSON.parse(latlon.responseText)[0].lat;
				lon = JSON.parse(latlon.responseText)[0].lon;
				
				initialize(lat, lon);
				document.getElementById('waiter').style.display = 'none';
				document.getElementById('map-canvas').style.visibility = 'visible';
			}

			function import_huissier(id)
			{
				exists = api_call_sync(query.get('server'),'optimus-avocats/'+query.get('owner')+'/contacts','GET',{"filters":[{"phone":huissiers.data[id].tel.replace(/\./g,'').replace(/\s/g,'')},{"categorie":60}]});
				if(exists.data.length > 0)
				{
					notification('Cet huissier existe déjà dans la base contact !','red');
					return false;
				}
				
				ville = api_call_sync('optimus-avocats.fr','commune/','GET',{"code_postal" : huissiers.data[id].cp, "nom" : huissiers.data[id].ville});
				api_call(query.get('server'), 'optimus-avocats/'+query.get('owner')+'/contacts', 'POST', 
				{
					"categorie":60,
					"lastname":"",
					"company_name": huissiers.data[id].nom.replace('Maître ','').replace('S.C.P','SCP').replace('S.E.L.A.R.L','SELARL'),
					"address": (huissiers.data[id].ads1 || '') + (huissiers.data[id].ads1 && huissiers.data[id].ads2 ? "\n" : '') + (huissiers.data[id].ads2 || '') + (huissiers.data[id].ads2 && huissiers.data[id].ads3 ? "\n" : '') + (huissiers.data[id].ads3 || '') + (ville.data[0] != null ? '' : "\n" + huissiers.data[id].cp + ' ' + huissiers.data[id].ville),
					"phone": huissiers.data[id].tel.replace(/\./g,'').replace(/\s/g,''),
					"fax": huissiers.data[id].fax.replace(/\./g,'').replace(/\s/g,''),
					"email": huissiers.data[id].em1.replace(/\./g,'').replace(/\s/g,''),
					"zipcode": (ville.data[0] != null ? ville.data[0].code_postal : null),
					"city_name": (ville.data[0] != null ? ville.data[0].nom : null),
					"city": (ville.data[0] != null ? ville.data[0].commune_insee : null)
				}
				, 'huissier_imported');
			}

			function huissier_imported()
			{
				notification('L\'huissier a été importé dans la base contact','green');
			}
			
			function initialize(lat, lon)
			{
				
				var mapOptions = 
				{
					center: new google.maps.LatLng(lat, lon),
					zoom: 11,
					mapTypeId: google.maps.MapTypeId.MAP,
					gestureHandling: 'greedy'
				};
				var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
				var marker = new google.maps.Marker(
				{
					position: new google.maps.LatLng(lat,lon),
					map: map,
					title:"CONTACT",
				});
				var image = {url: '/modules/contacts/marker_huissier.png', size: new google.maps.Size(20, 32), origin: new google.maps.Point(0, 0), anchor: new google.maps.Point(10, 32)};
				
				for (i=0; i < huissiers.data.length; i++)
				{
					var marker = new google.maps.Marker(
					{
						position: new google.maps.LatLng(huissiers.data[i].lat, huissiers.data[i].lon),
						map: map,
						icon: image,
						animation: google.maps.Animation.DROP,
						title: huissiers.data[i].nom,
						detail: huissiers.data[i].nom + '<br/><br/>'
						+ (huissiers.data[i].ads1 != '' ? huissiers.data[i].adsl + '<br/>':'') 
						+ (huissiers.data[i].ads2 != '' ? huissiers.data[i].ads2 + '<br/>' : '') 
						+ (huissiers.data[i].ads3 != '' ? huissiers.data[i].ads3 + '<br/>' : '') 
						+ huissiers.data[i].cp + ' ' + huissiers.data[i].ville + '<br/><br/>' 
						+ (huissiers.data[i].tel != '' ? 'Tél : <a href="tel:' + huissiers.data[i].tel + '">' + huissiers.data[i].tel + '</a><br/>':'') 
						+ (huissiers.data[i].fax != '' ? 'Fax : ' + huissiers.data[i].fax + '<br/>':'') + '<br/>' 
						+ (huissiers.data[i].em1 != '' ? 'Email : ' + huissiers.data[i].em1 + '<br/>' : '') 
						+ (huissiers.data[i].web != '' ? '<br/>Site : <a href="#" onclick="window.open(\'' + (huissiers.data[i].web !== false ? huissiers.data[i].web : 'https://' + huissiers.data[i].web) + '\')">' + huissiers.data[i].web + '</a><br/>' : '') 
						+ '<br/>'
						+ '<button onclick="curtain_close();alert_close()">OK</button><br/><img src="/lib/fontawesome/download.svg" style="width:16px;float:right;cursor:pointer" onclick="import_huissier(' + i + ')"/>'
					 }
					);
					marker.addListener('click', function() {curtain_open();alert_open();alertbox.innerHTML = '<div>'+this.detail+'</div>';});
				}
			}
			
			var update_link = new BroadcastChannel('update_link');
			update_link.onmessage = function (ev) {window.location.reload()}
			window.onblur = function(){update_link.postMessage('update_link')}
		</script>
	</head>
	
	<body align="center" style="height:100%">
		<div id="waiter" style="margin:40px">Localisation des huissiers compétents en cours<br/><img src="/images/waiter.gif"/></div>
		<div id="map-canvas" align="center" style="height:100%;width:100%;visibility:hidden"></div>
	</body>
</html>