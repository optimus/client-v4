<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>RELEVES</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/releves/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>
	<div id="search_container" style="width:95%; margin:auto; margin-top: 40px; display:flex; flex-wrap: wrap; gap: 20px; justify-content:center; align-items: center">

		<div>
			COMPTES<br/>
			<table id="comptes" class="statictable">
				<thead>
					<tr>
						<td></td>
						<td>BANQUE</td>
						<td>COMPTE</td>
						<td>SOLDE</td>
						<td>MISE A JOUR</td>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		
		<div>
			STRUCTURE<br/>
			<select id="structures" onchange="get_comptes();get_operations()"></select><br/>	
			<br/>
			RECHERCHE<br/>
			<input id="search" type="search" onsearch="get_operations()" onclick="get_operation()" autofocus spellcheck="false" autocomplete="off" incremental="incremental">
		</div>

		<div>
			PERIODE DU<br/>
			<input id="start" type="date" onchange="get_operations()"><br/>
			<br/>
			AU<br/>
			<input id="end" type="date"  onchange="get_operations()"><br/>
		</div>

		<div>
			ANNEE<br/>
			<input id="year" type="number" step="1" min="1900" onchange="set_dates(); get_operations()"></select><br/>
			<br/>
			MOIS<br/>
			<select id="month" onchange="set_dates(); get_operations()">
				<option value=""></option>
				<option value="0">Janvier</option>
				<option value="1">Février</option>
				<option value="2">Mars</option>
				<option value="3">Avril</option>
				<option value="4">Mai</option>
				<option value="5">Juin</option>
				<option value="6">Juillet</option>
				<option value="7">Août</option>
				<option value="8">Septembre</option>
				<option value="9">Octobre</option>
				<option value="10">Novembre</option>
				<option value="11">Décembre</option>
			</select>
		</div>

		<div>
			COMPTABILISE<br/>
			<select id="counted" onchange="get_operations()">
				<option value="all">Tout</option>
				<option value="yes">Comptabilisé</option>
				<option value="no">Non comptabilisé</option>
			</select><br/>
			<br/>
			DEBIT/CREDIT<br/>
			<select id="montant" onchange="get_operations()">
				<option value="all">Tout</option>
				<option value="negative">Débit</option>
				<option value="positive">Crédit</option>
			</select><br/>
		</div>
	
	</div>

	<table id="operations" class="statictable" style="margin-top:40px">
		<thead>
			<tr>
				<td>ID</td>
				<td>DATE &darr;</td>
				<td>A VENTILER</td>
				<td>MONTANT</td>
				<td width="800">DESCRIPTION</td>
				<td width="200">COMPTE</td>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</body>

<script>
const query = new URLSearchParams(window.location.search);
document.getElementById('year').max = document.getElementById('year').value = new Date().getFullYear()


api_call(query.get('server'),'allspark/logged', 'GET', {}, 'init');
function init(response)
{
	structures = api_call_sync(query.get('server'),'optimus-avocats/' +get_cookie('user_id') + '/structures','GET',{});
	for (structure of structures.data)
	{
		user = api_call_sync(structure.server,'allspark/users/' + structure.user,'GET',{});
		document.getElementById('structures').options[document.getElementById('structures').options.length] = new Option([user.data.firstname,user.data.lastname].filter(Boolean).join(' '), structure.user);
	}
		
	for (structure of structures.data)
		if (!structure.sortie)
			break;
	document.getElementById('structures').value = structure.user;

	get_comptes()
	get_operations()
}

function get_comptes()
{
	document.getElementById('comptes').tBodies[0].innerHTML = ''
	comptes = api_call_sync(query.get('server'), 'optimus-structures/' + document.getElementById('structures').value + '/comptes', 'GET', {})
	for (let compte of comptes.data)
	{
		tr = document.getElementById('comptes').tBodies[0].insertRow()
		tr.id = compte.id

		td = tr.insertCell()
		checkbox = document.createElement('input')
		checkbox.type = 'checkbox'
		checkbox.checked = true
		checkbox.onclick = () => get_operations()
		td.appendChild(checkbox)

		td = tr.insertCell()
		td.style.textAlign = 'left'
		td.innerText = compte.banque

		td = tr.insertCell()
		td.style.textAlign = 'left'
		td.innerText = compte.compte

		td = tr.insertCell()
		td.style.textAlign = 'right'
		td.innerText = Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'EUR'}).format(compte.solde)

		td = tr.insertCell()
		td.style.textAlign = 'right'
		td.innerText = new Date(compte.lastupdated).toLocaleDateString() + ' ' + new Date(compte.lastupdated).toLocaleTimeString()
	}
}

function get_operations()
{
	document.getElementById('operations').tBodies[0].innerHTML = ''

	let search_parameters = 
	{
		"fields": 
		[
			{name: "id"},
			{name: "date", sort_rank: 0, sort_order: "DESC"},
			{name: "montant"},
			{name: "description"},
			{name: "compte"}
		],
		"advanced_search": [],
		"global_search": document.getElementById('search').value,
	}
	
	for (row of document.getElementById('comptes').tBodies[0].rows)
		if (row.cells[0].firstChild.checked == false)
			search_parameters.advanced_search.push({field: "compte", operator: "<>", value: row.id, next: "AND"})

	if (document.getElementById('start').value)
		search_parameters.advanced_search.push({field: "date", operator: ">=", value: document.getElementById('start').value, next: "AND"})

	if (document.getElementById('end').value)
		search_parameters.advanced_search.push({field: "date", operator: "<=", value: document.getElementById('end').value, next: "AND"})
	
	if (document.getElementById('montant').value == 'positive')
		search_parameters.advanced_search.push({field: "montant", operator: ">=", value: 0, next: "AND"})
	else if (document.getElementById('montant').value == 'negative')
		search_parameters.advanced_search.push({field: "montant", operator: "<=", value: 0, next: "AND"})

	operations = api_call_sync(query.get('server'), 'optimus-structures/' + document.getElementById('structures').value + '/operations', 'GET', search_parameters)
	for (let operation of operations.data)
	{
		tr = document.getElementById('operations').tBodies[0].insertRow()
		tr.id = operation.id
		tr.style = 'cursor:pointer'

		td = tr.insertCell()
		td.style.textAlign = 'right'
		td.innerText = operation.id

		td = tr.insertCell()
		td.innerText =  new Date(operation.date).toLocaleDateString()

		td = tr.insertCell()
		td.style.textAlign = 'right'
		td.innerText = Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'EUR'}).format(operation.montant)

		td = tr.insertCell()
		td.style.textAlign = 'right'
		td.innerText = Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'EUR'}).format(operation.montant)

		td = tr.insertCell()
		td.style.textAlign = 'left'
		td.innerText = operation.description.replace(/(?:\r\n|\r|\n)/g, ' - ')

		td = tr.insertCell()
		td.innerText = comptes.data.find(compte => compte.id == operation.compte).banque
	}
}

function set_dates()
{
	document.getElementById('start').value = new Date(Date.UTC(document.getElementById('year').value, document.getElementById('month').value || 0, 1)).toISOString().substring(0,10)
	document.getElementById('end').value = new Date(Date.UTC(document.getElementById('year').value, parseInt(document.getElementById('month').value || 11)+1, 0)).toISOString().substring(0,10)
}
</script>

</html>