<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>MON COMPTE</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/server-users/favicon-32x32.png">
	<script src="/js/cookies.js"></script>
	<script type="text/javascript">
		const query = new URLSearchParams(window.location.search);
		window.location = '/modules/server-users/editor.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+get_cookie('user_id');
	</script>
</head>

</html>