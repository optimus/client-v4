<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>COMPTES BANCAIRES</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/comptes-bancaires/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/dynamictable.js"></script>
	<script src="/js/number_format.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>
	STRUCTURE<br/>
	<select id="structures" style="width:auto" onchange="get_comptes(this.value)"></select><br/>
	<br/>
	NORDIGEN secret_id<br/>
	<input id="secret_id" style="width:250px"><br/>
	<br/>
	NORDIGEN secret_key<br/>
	<input id="secret_key" style="width:900px"><br/>
	<br/>
	<button onclick="list_bank()">Ajouter un compte</button><br/>
	<br/>
	<br/>
	<ul id="banques" style="width:95%; margin:auto; display:grid; grid-template-columns:repeat(auto-fit, minmax(150px, max-content)); grid-gap:20px; justify-content:center; padding:initial">

	</ul>
	<br/>
	<table id="comptes" class="statictable">
		<thead>
			<tr>
				<td>ID</td>
				<td>IBAN</td>
				<td>BANQUE</td>
				<td>COMPTE</td>
				<td>SOLDE</td>
				<td>IMPORT</td>
				<td>RELEVES</td>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</body>

<script>
const query = new URLSearchParams(window.location.search);

api_call(query.get('server'),'allspark/logged', 'GET', {}, 'init');
function init(response)
{
	structures = api_call_sync(query.get('server'),'optimus-avocats/' +get_cookie('user_id') + '/structures','GET',{});
	for (structure of structures.data)
	{
		user = api_call_sync(structure.server,'allspark/users/' + structure.user,'GET',{});
		document.getElementById('structures').options[document.getElementById('structures').options.length] = new Option([user.data.firstname,user.data.lastname].filter(Boolean).join(' '), structure.user);
	}
		
	for (structure of structures.data)
		if (!structure.sortie)
			break;
	document.getElementById('structures').value = structure.user;

	preferences = api_call_sync(query.get('server'), 'allspark/preferences', 'GET', {'owner':45, 'user':2, 'module':'comptes_bancaires', 'preference':'nordigen_id'})
	document.getElementById('secret_id').value = preferences?.data?.comptes_bancaires?.nordigen_id?.secret_id || ''
	document.getElementById('secret_key').value = preferences?.data?.comptes_bancaires?.nordigen_id?.secret_key || ''

	get_comptes(structure.user)
}

function get_comptes(structure_id)
{
	document.getElementById('comptes').tBodies[0].innerHTML = ''
	comptes = api_call_sync(query.get('server'), 'optimus-structures/' + structure_id + '/comptes', 'GET', {})
	for (let compte of comptes.data)
	{
		tr = document.getElementById('comptes').tBodies[0].insertRow()
		tr.style = 'cursor:pointer'

		td = tr.insertCell()
		td.innerText = compte.id

		td = tr.insertCell()
		td.innerText = compte.iban

		td = tr.insertCell()
		td.innerText = compte.banque

		td = tr.insertCell()
		td.innerText = compte.compte

		td = tr.insertCell()
		td.innerText = compte.solde

		td = tr.insertCell()
		td.id = structure_id

		button = document.createElement('button')
		button.innerText = 'IMPORT'
		button.onclick = function() {window.open('https://api.' + query.get('server') + '/optimus-structures/' + this.parentNode.id + '/nordigen-transactions/' + compte.id)}
		td.appendChild(button)

		td = tr.insertCell()

		button = document.createElement('button')
		button.innerText = 'RELEVES'
		button.onclick = function() {window.open('/modules/releves/index.php?server=' + query.get('server') + '&owner=' + query.get('owner'))}
		td.appendChild(button)
	}
}

function list_bank()
{
	document.getElementById('banques').innerHTML = '<li style="border:1px solid #000000;padding:10px;width:150px;height:150px;background:#FFFFFF;list-style-type:none; cursor:pointer" onclick="add_bank(\'SANDBOXFINANCE_SFIN0000\')"><img src="https://sandboxfinance.nordigen.com/static/assets/img/sandbox_finance.svg" style="height:100px; width:100px"><br/>SANDBOX FINANCE</li>'
	banques = api_call_sync(query.get('server'), 'optimus-structures/' + query.get('owner') + '/nordigen-banques', 'GET', {})
	for (let banque of banques.data)
	{
		li = document.createElement('li')
		li.id = banque.id
		li.title = banque.transaction_total_days
		li.style = 'border:1px solid #000000;padding:10px;width:150px;height:150px;background:#FFFFFF;list-style-type:none; cursor:pointer'
		li.onclick = function(){add_bank(banque.id)}
		document.getElementById('banques').appendChild(li)
		img = document.createElement('img')
		img.src = banque.logo
		img.style = 'height:100px; width:100px'
		li.appendChild(img)
		li.innerHTML += '<br/>' + banque.name
	}
}

function add_bank(banque_id)
{
	agreement = api_call_sync(query.get('server'), 'optimus-structures/' + document.getElementById('structures').value + '/nordigen-requisition/' + banque_id.toLowerCase(), 'GET', {})
	console.log(agreement.data)
	window.open(agreement.data.link)
}

</script>

</html>