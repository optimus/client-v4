<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta name="viewport" content="width=410, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="/modules/server-users/favicon-32x32.png" />
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/tabs.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
	</head>

	<body style="text-align:center;overflow-y:hidden" onload="load_tab()">
		<div id="tabs" class="tabs" onclick="if (event.target.name=='tabs') document.getElementById('0').checked = false">
			<input id="0" type="checkbox"/><label for="0"></label>
			<input id="general" type="radio" name="tabs" onclick="load_tab()" link="tab_general.php" checked/><label for="general">GENERAL</label>
			<input id="services" type="radio" name="tabs" onclick="load_tab()" link="tab_services.php"/><label for="services">SERVICES</label>
			<input id="courriel" type="radio" name="tabs" onclick="load_tab()" link="tab_courriel.php"/><label for="courriel">COURRIEL</label>
			<input id="partages" type="radio" name="tabs" onclick="load_tab()" link="tab_partages.php"/><label for="partages">PARTAGES</label>
			<input id="associes" type="radio" name="tabs" onclick="load_tab()" link="tab_associes.php" style="display:none"/><label for="associes" style="display:none">ASSOCIES</label>
			<input id="serveurs" type="radio" name="tabs" onclick="load_tab()" link="tab_serveurs.php"/><label for="serveurs">SERVEURS SECONDAIRES</label>
		</div>
		<div class="tab_separator"></div>
		<iframe id="frame" marginwidth="0" marginheight="0" frameborder="0" onload="this.style.height = (window.innerHeight - 57) + 'px'" style="width:100%"></iframe>
	</body>

	<script>
		const query = new URLSearchParams(window.location.search);
		users_services = api_call_sync(query.get('server'),'allspark/users_services/'+query.get('id'),'GET', {});
		for (users_service of users_services.data)
			if (users_service.name == 'optimus-structures')
			{
				document.getElementById('associes').labels[0].style.display = '';
				document.getElementById('associes').style.display = '';
			}
	</script>

</html>