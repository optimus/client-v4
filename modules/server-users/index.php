<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>UTILISATEURS</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/server-users/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<input type="button" value="AJOUTER UN UTILISATEUR" style="margin:20px" onclick="api_call(query.get('server'),'allspark/users', 'POST', {'email':window.prompt('Email ?').toLowerCase(),'status':1,'admin':0, 'server':window.prompt('Serveur ?',get_cookie('server')), 'firstname':window.prompt('Prénom ?'), 'lastname':window.prompt('Nom de famille ?')}, 'user_created')"/>
	
	<table id="statictable" class="statictable">
		<thead>
			<tr>
				<td>ID</td>
				<td>PRÉNOM</td>
				<td>NOM</td>
				<td>EMAIL</td>
				<td>STATUS</td>
				<td>ADMIN</td>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
	
	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		
		function user_created(user)
		{
			editor = window.open('/modules/server-users/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+user.data.id,'editor');
			window.location.reload();
		}
		
		users = api_call_sync(query.get('server'),'allspark/users', 'GET',{});
		for (i=0; i < users.data.length; i++)
		{
			tr = document.getElementById('statictable').tBodies[0].insertRow();
			tr.id = users.data[i].id;
			tr.style.fontSize = '14px';
			
			td1 = tr.insertCell();
			td1.style.textAlign = 'right';
			td1.style.width = '50px';
			td1.innerHTML = users.data[i].id;

			td2 = tr.insertCell();
			td2.style.textAlign = 'left';
			td2.style.width = '100px';
			td2.innerHTML = users.data[i].firstname;

			td3 = tr.insertCell();
			td3.style.textAlign = 'left';
			td3.style.width = '100px';
			td3.innerHTML = users.data[i].lastname;

			td4 = tr.insertCell();
			td4.style.textAlign = 'left';
			td4.style.width = '250px';
			td4.style.color = '#0000FF';
			td4.style.cursor = 'pointer';
			td4.innerHTML = users.data[i].email;
			td4.onclick = function(){window.open('/modules/server-users/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+this.parentNode.id,'editor');};
			td4.innerHTML = users.data[i].email;

			td5 = tr.insertCell();
			td5.style.width = '50px';
			td5.innerHTML = users.data[i].status == 1 ? '<img src="/images/checked.gif"/>' : '';

			td6 = tr.insertCell();
			td6.style.width = '50px';
			td6.innerHTML = users.data[i].admin == 1 ? '<img src="/images/checked.gif"/>' : '';
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
	</script>
</body>
</html>