<!DOCTYPE html>
<html lang="fr">

<head>
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/editor.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div id="container" class="flex_container" style="max-width:926px;margin:auto;display:none">

		<div class="editor" style="min-width:410px">
			<div>PARTAGES</div>
			<div style="text-align:center;line-height:18px">
				Les utilisateurs renseignés ci-dessous ont un accès complet<br/>en lecture et écriture sur les données de :<br/><span id="user"></span> : <br/>
				<table id="shares"></table>
				<button id="add_share_button" onclick="add_share({})" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un utilisateur</button>
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'PARTAGES';
		
		users = {};
		
		api_call(query.get('server'),'allspark/logged','GET', {},'init');
		function init(response)
		{
			users = api_call_sync(query.get('server'),'allspark/users','GET', {});
			document.getElementById('user').innerHTML = users.data.filter(user => user.id == query.get('id'))[0].email;
			shares = api_call_sync(query.get('server'),'allspark/authorizations','GET', {'owner':query.get('id'), 'resource':'*'});
			shares.data.sort((a,b) => a.id - b.id);
			if (shares.data)
				for(share of shares.data)
					add_share(share);
			document.getElementById('container').style.display = '';
		}

		function populate(object)
		{
			lastvalue = object.value;
			object.innerHTML = '';
			existing_shares = [];
			for (i=0; i<document.getElementById('shares').getElementsByTagName('select').length; i++)
				existing_shares[existing_shares.length] = document.getElementById('shares').getElementsByTagName('select')[i].value;
			for(user of users.data)
				if (user.id != query.get('id') && !existing_shares.includes(user.id))
					object.add(new Option(user.firstname && user.lastname ? user.firstname + ' ' + user.lastname : user.email, user.id));
			object.value = lastvalue;
			this.focus();
		}

		function add_share(share)
		{
			tr = document.getElementById('shares').insertRow();
			td = tr.insertCell();
			td.id = share.id;
			td.style.textAlign = 'center';
			select = document.createElement('select');
			td.appendChild(select);
			select.id = 'select_'+share.id
			select.style.width = '348px';
			select.onclick = function(){populate(this)}
			select.onchange = function()
			{
				if (this.value == this.lastvalue)
				{
					this.blur();
					return false;
				}
					
				if (this.parentNode.id == 'undefined')
				{
					result = api_call_sync(query.get('server'),'allspark/authorizations','POST', {'owner':query.get('id'),'user':this.value,'resource':'*','read':1,'write':1,'create':1,'delete':1});
					if (result.code == 201)
					{
						this.parentNode.id = result.data.id;
						this.lastvalue = this.value;
						this.blur();
						return true;
					}
					else
						return false;
				}
				else
				{
					result = api_call_sync(query.get('server'),'allspark/authorizations/'+this.parentNode.id,'PATCH', {'user':this.value,'read':1,'write':1,'create':1,'delete':1});
					if (result.code == 200)
					{
						this.lastvalue = this.value;
						this.blur();
						return true;
					}
					else
					{
						this.value = this.lastvalue;
						return false;
					}
						
				}
			}
			select.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			select.onfocus = function(){this.classList.add('editing')}
			select.onblur = function(){this.classList.remove('editing')}
			if (!share.id)
				select.add(new Option('',0));
			else
				for(user of users.data)
					if (user.id == share.user)
						select.add(new Option(user.firstname && user.lastname ? user.firstname + ' ' + user.lastname : user.email, user.id));
			select.value = share.user || 0;
			select.lastvalue = share.user || 0;
			if (share.id == 0)
				select.disabled = true;

			deleter = document.createElement('img');
			deleter.src = '/lib/fontawesome/trash.svg';
			deleter.style = 'height:18px;vertical-align:-4px;margin-left:4px;filter:contrast(60%);cursor:pointer';
			deleter.onclick = function()
			{
				if (this.parentNode.firstChild.value==0)
					this.parentNode.parentNode.remove();
				else
				{
					result = api_call_sync(query.get('server'),'allspark/authorizations/'+this.parentNode.id,'DELETE', {});
					if (result.code == 200)
					{
						this.parentNode.parentNode.remove();
						document.getElementById('add_share_button').style.visibility = '';
					}
				}
			};
			td.appendChild(deleter);
			if (share.id == 0)
				deleter.style.visibility = 'hidden';
			if (document.getElementById('shares').rows.length == users.data.length)
				document.getElementById('add_share_button').style.visibility = 'hidden';
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>