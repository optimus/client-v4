<!DOCTYPE html>
<html lang="fr">

<head>
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/editor.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div id="container" class="flex_container" style="max-width:926px;margin:auto;display:none">

		<div class="editor" style="min-width:410px">
			<div>SERVEURS SECONDAIRES</div>
			<div style="text-align:center;line-height:18px">
				Si vous disposez d'un accès<br/>
				sur un autre serveur ALLSPARK,<br/>
				renseignez son adresse ci-dessous pour le connecter :<br/>
				<br/>
				<table id="servers" style="width:100%">
					<tr>
						<td align="center"><input id="default" style="width:250px;text-align:center;margin-right:22px" disabled readonly/></td>
					</tr>
				</table>
				<br/>
				<button onclick="add_server({})" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un serveur</button>
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'SERVEURS';
		users = {};
		
		api_call(query.get('server'),'allspark/logged','GET', {},'init');
		function init(response)
		{
			user = api_call_sync(query.get('server'),'allspark/users/'+response.data.id,'GET', {});
			document.getElementById('default').value = user.data.server;
			
			servers = api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/servers','GET', {});
			if (servers.data)
				for(server of servers.data)
					add_server(server);
			document.getElementById('container').style.display = '';
		}

		function add_server(server)
		{
			tr = document.getElementById('servers').insertRow();
			td = tr.insertCell();
			td.id = server.name;
			td.style.textAlign = 'center';
			input = document.createElement('input');
			input.style.width = '250px';
			input.style.textAlign = 'center';
			td.appendChild(input);
	
			input.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			input.onfocus = function(){this.classList.add('editing')}
			input.onblur = function()
			{
				if (this.parentNode.id == 'undefined' && this.value == '')
					this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				
				if (this.value == this.lastvalue)
				{
					this.blur();
					this.classList.remove('editing');
					return false;
				}
					
				if (this.parentNode.id == 'undefined')
				{
					result = api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/servers','POST', {'server':this.value});
					if (result.code == 201)
					{
						this.parentNode.id = result.data.server;
						this.lastvalue = this.value;
						this.classList.remove('editing');
						return true;
					}
					else
						return false;
				}
				else
				{
					result = api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/servers','PATCH', {'user':query.get('id'),'server':this.value, 'oldvalue':this.lastvalue});
					if (result.code == 200)
					{
						this.lastvalue = this.value;
						this.classList.remove('editing');
						return true;
					}
					else
					{
						this.value = this.lastvalue;
						return false;
					}
						
				}
			}
			
			input.value = server.server || '';
			input.lastvalue = server.server || '';
			input.focus();

			deleter = document.createElement('img');
			deleter.src = '/lib/fontawesome/trash.svg';
			deleter.style = 'height:18px;vertical-align:-4px;margin-left:4px;filter:contrast(60%);cursor:pointer';
			deleter.onclick = function()
			{
				result = api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/servers','DELETE', {'server':this.parentNode.firstChild.value});
				if (result.code == 200)
					this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
			};
			td.appendChild(deleter);
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>