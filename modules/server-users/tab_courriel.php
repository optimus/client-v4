<!DOCTYPE html>
<html lang="fr">

<head>
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div id="container" class="flex_container" style="max-width:926px;margin:auto;display:none">

		<div class="flex_container" style="max-width:926px;margin:auto">

		<div class="editor" style="min-width:410px">
			<div id="aliases_title">ALIASES</div>
			<div style="text-align:center;line-height:18px">
				<table id="aliases">
					<tr>
						<td>
							<img src="aliases.svg" style="height:60px;padding:0 12px"/>
						</td>
						<td style="width:270px;text-align:left">
							<i>Les courriels envoyés sur les adresses ci-dessous seront automatiquement redirigés vers <span name="user_email"></span></i><br/>
						</td>
					</tr>
				</table>
				<button onclick="add_email('aliases')" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un alias</button>
			</div>
		</div>

		<div class="editor" style="min-width:410px">
			<div id="redirections_title">REDIRECTIONS</div>
			<div style="text-align:center;line-height:18px">
				<table id="redirections">
					<tr>
						<td>
							<img src="redirections.svg" style="height:60px;padding:0 12px"/>
						</td>
						<td style="width:270px;text-align:left">
							<i>Les courriels adressés à <span name="user_email"></span> sont automatiquement redirigés sur les adresses ci-dessous :</i><br/>
						</td>
					</tr>
				</table>
				<button onclick="add_email('redirections')" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter une redirection</button>
			</div>
		</div>

		<div class="editor" style="min-width:410px">
			<div id="sender_bcc_title">COPIE DES MESSAGES ENVOYES</div>
			<div style="text-align:center;line-height:18px">
				<table id="sender_bcc">
					<tr>
						<td>
							<img src="sender_bcc.svg" style="height:60px;padding:0 12px"/>
						</td>
						<td style="width:270px;text-align:left">
							<i>Une copie de chaque courriel envoyé par <span name="user_email"></span> est adressée en copie cachée aux destinataires ci-dessous :</i><br/>
						</td>
					</tr>
				</table>
				<button onclick="add_email('sender_bcc')" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un destinataire</button>
			</div>
		</div>

		<div class="editor" style="min-width:410px">
			<div id="recipient_bcc_title">COPIE DES MESSAGES RECUS</div>
			<div style="text-align:center;line-height:18px">
				<table id="recipient_bcc">
					<tr>
						<td>
							<img src="recipient_bcc.svg" style="height:60px;padding:0 12px"/>
						</td>
						<td style="width:270px;text-align:left">
							<i>Une copie de chaque courriel reçu par <span name="user_email"></span> est envoyée en copie cachée aux destinataires ci-dessous :</i><br/>
						</td>
					</tr>
				</table>
				<button onclick="add_email('recipient_bcc')" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un destinataire</button>
			</div>
		</div>

		<div id="quota_editor" class="editor" style="min-width:410px;display:none">
			<div id="quota_title">ESPACE ALLOUÉ</div>
			<div style="text-align:center;line-height:18px">
				Indiquez l'espace disque alloué à la boite mail en GigaBytes.<br/>
				Pour une taille illimitée, indiquez "0".<br/><br/>
				<input id="quota" type="number" step="0.01" min="0" max="999" style="text-align:right" onblur="api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/mailbox','PATCH', {'quota':(this.value*1024*1024*1024).toFixed(2)});"/> Gigabytes
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'COURRIEL';

		api_call(query.get('server'),'allspark/users/'+query.get('id'),'GET', {},'init');
		function init(response)
		{
			itm = response.data;
			document.getElementById('container').style.display = '';
			parent.window.document.title = itm.email;
			for (span of document.getElementsByName('user_email'))
				span.innerHTML = itm.email;
			mailbox = api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/mailbox','GET', {});
			if (mailbox.code != 200)
				return false;
			
			if (mailbox?.data?.aliases)
				for( address of mailbox.data.aliases.split(';'))
					add_email('aliases',address);
			if (mailbox?.data?.redirections)
				for( address of mailbox.data.redirections.split(';'))
					add_email('redirections',address);
			if (mailbox?.data?.sender_bcc)
				for( address of mailbox.data.sender_bcc.split(';'))
					add_email('sender_bcc',address);
			if (mailbox?.data?.recipient_bcc)
				for( address of mailbox.data.recipient_bcc.split(';'))
					add_email('recipient_bcc',address);
			if (get_cookie('user_admin') == 1)
			{
				document.getElementById('quota_editor').style.display = 'block';
				document.getElementById('quota').value =(mailbox?.data?.quota/1024/1024/1024).toFixed(2);
			}
			
			
		}

		function add_email(module,value)
		{
			tr = document.getElementById(module).insertRow();
			td = tr.insertCell();
			td.colSpan = 2;
			td.style.textAlign = 'center';
			input = document.createElement('input');
			input.style.width = '348px';
			input.type = 'text';
			input.name = module;
			input.value = value || null;
			input.onblur = function(){save(module, this)}
			input.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			input.onfocus = function(){this.classList.add('editing');document.getElementById(module+'_title').style.color='#FF0000'};
			td.appendChild(input);
			if (!value)
				input.focus();
			deleter = document.createElement('img');
			deleter.src = '/lib/fontawesome/trash.svg';
			deleter.style = 'height:18px;vertical-align:-4px;margin-left:4px;filter:contrast(60%);cursor:pointer';
			deleter.onclick = function(){this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);save(module)};
			td.appendChild(deleter);
		}

		function save(module, object)
		{
			emailarray = [];
			var value = new Object();
			value[module] = null;
			for (input of document.getElementsByName(module))
			{
				input.value = input.value.toLowerCase();
				if (input.value == '')
					input.parentNode.parentNode.parentNode.removeChild(input.parentNode.parentNode);
				else
				{
					emailarray[emailarray.length] = input.value;
					value[module] = emailarray.join(';')
				}
			}
			result = api_call_sync(query.get('server'),'allspark/users/'+query.get('id')+'/mailbox','PATCH',value);
			if (result.code == 200)
			{
				document.getElementById(module+'_title').style.color='#FFFFFF';
				object.classList.remove('editing');
			}
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>