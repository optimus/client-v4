<!DOCTYPE html>
<html lang="fr">

<head>
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/editor.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div id="container" class="flex_container" style="max-width:926px;margin:auto">

		<div class="editor" style="min-width:410px">
			<div>GENERAL</div>
			<div>
				ID : <input disabled id="id" type="text" tabindex="1" style="width:274px"/><br/>
				Serveur : <input id="server" type="text" maxlength="128" tabindex="2" style="width:274px" onblur="simple_save(this)"/><br/>
				Email : <input disabled id="email" type="text" maxlength="128" tabindex="3" style="width:274px"/><br/>
				Password : <input id="password" type="password" tabindex="4" style="width:253px" onblur="simple_save(this)"/>&nbsp;<img src="/lib/fontawesome/eye.svg" style="width:18px;filter:contrast(60%);vertical-align:-4px" onclick="document.getElementById('password').type = document.getElementById('password').type=='text'?'password':'text'"><br/>
				Prénom : <input id="firstname" type="text" maxlength="128" tabindex="5" style="width:274px" onblur="simple_save(this)"/><br/>
				Nom : <input id="lastname" type="text" maxlength="128" tabindex="6" style="width:274px" onblur="simple_save(this)"/><br/>
				Status : <label class="switch" style="margin-right:242px"><input id="status" type="checkbox" tabindex="7" onchange="simple_save(this)"><span class="slider round"></span></label><br/>
				Admin : <label class="switch"  style="margin-right:242px"><input id="admin" type="checkbox" tabindex="8" onchange="change_admin(this)"><span class="slider round"></span></label><br/>
				<div style="text-align:center">
					<button id="user_delete" onclick="user_delete()"><img src="/lib/fontawesome/trash.svg" style="height:16px;opacity:0.6;vertical-align:-4px"/>&nbsp;&nbsp;<span style="color:#FF0000">Supprimer</span></button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button onclick="parent.window.open('about:blank', '_self');parent.window.close()"><img src="/lib/fontawesome/times.svg" style="width:14px;filter:contrast(60%);vertical-align:-6px">&nbsp;&nbsp;Quitter</button>
				</div>
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'MON COMPTE';

		module = 'users';
		itm = {};
		authorizations = {};

		api_call(query.get('server'),'allspark/users/'+query.get('id'),'GET', {},'init');
		function init(response)
		{
			itm = response.data;
			authorizations = {"read":1,"write":1,"create":1,"delete":1};
			editor_init();
			parent.window.document.title = itm.email;
			if (get_cookie('user_admin')==0) document.getElementById('status').setAttribute('disabled','true');
			if (get_cookie('user_admin')==0) document.getElementById('admin').setAttribute('disabled','true');
			if (get_cookie('user_admin')==0 && get_cookie('user_id')!=query.get('id')) document.getElementById('password').setAttribute('disabled','true');
			document.getElementById('container').style.display = '';
		}

		function simple_save(obj)
		{
			if(obj.type == 'checkbox' && obj.checked)
				value = '1';
			else if(obj.type == 'checkbox' && !obj.checked)
				value = '0';
			else
				value = obj.value

			if (itm[obj.id] != value)
			{
				api_call(query.get('server'),'allspark/users/'+query.get('id'),'PATCH',{[obj.id]:value},'updated',obj);
				itm[obj.id] = value;
			}
			else
			{
				obj.classList.remove('editing');
				obj.blur();
			}
		}

		function user_delete()
		{
			if(confirm('Etes vous sûr de vouloir supprimer cet utilisateur ?\nToutes ses données seront supprimées !'))
				api_call(query.get('server'),'allspark/users/'+itm.id,'DELETE',{}, 'user_deleted');
		}

		function user_deleted(response)
		{
			if (response.code == 200)
			{
				parent.window.open('about:blank', '_self');
				parent.window.close();
			}
		}

		function change_admin(checkbox)
		{
			if (get_cookie('user_id')==query.get('id') && checkbox.checked==false) 
			{
				if (confirm('Etes vous sûr de vouloir supprimer vos propres droits administrateur ?\nSeul un autre administrateur pourra les restaurer')) 
				{
					simple_save(checkbox);
					delete_cookie('user_id');
					delete_cookie('user_admin');
					setTimeout(function(){api_call(get_cookie('server'),'allspark/logout','GET',{});window.location.reload()},100);
				}
				else
				{
					checkbox.checked=true;
					return false;
				}
			}
			else
				simple_save(checkbox);
		}

		if (get_cookie('user') == 'demo@demoptimus.fr')
		{
			if (query.get('id') <= 3)
			{
				document.getElementById('password').disabled = true
				alert('le mot de passe du compte demo ne peut pas être modifié')
				document.getElementById('user_delete').style.display = 'none';
			}
		}
			
		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>