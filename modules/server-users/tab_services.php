<!DOCTYPE html>
<html lang="fr">

<head>
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/editor.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div id="container" class="flex_container" style="max-width:926px;margin:auto">

		<div class="editor">
			<div>SERVICES</div>
			<div style="text-align:left">
				<table id="services" style="border-spacing:20px 0px"></table>
			</div>
		</div>

		<div class="editor">
			<div>AIDE</div>
			<div style="text-align:left;line-height:16px;text-align:justify">
				"Optimus Avocats" intègre les modules de gestion des dossiers et fiches d'intervention.<br/><br/>
				"Optimus Structures" intègre les modules de facturation et de gestion comptable.<br/><br/>
				Pour un cabinet individuel, il faut activer les 2 modules.<br/><br/>
				Pour une structure comprenant plusieurs associés, il est conseillé de créer un utilisateur distinct pour la structure, d'une part, et autant d'utilisateur avocats que d'associés.
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'SERVICES';

		api_call(query.get('server'),'allspark/logged','GET', {},'init');
		function init(response)
		{
			services = api_call_sync(query.get('server'),'allspark/services','GET', {});
			services = services.data.filter(data => data.name != 'allspark');
			users_services = api_call_sync(query.get('server'),'allspark/users_services/'+query.get('id'),'GET', {});
			for (service of services)
			{
				tr = document.getElementById('services').insertRow();
				tr.id = service.name;
				tr.version = service.version;
				td1 = tr.insertCell();
				td1.innerHTML = service.displayname;
				td2 = tr.insertCell();
				td2.style.width = '40px';
				td3 = tr.insertCell();
				label = document.createElement('label');
				label.classList.add('switch');
				td3.appendChild(label);
				input = document.createElement('input');
				input.type = 'checkbox';
				input.onclick = function()
				{
					if (!this.checked)
						if (window.confirm('Etes-vous sûr ? Cette opération supprimera les données de l\'utilisateur'))
						{
							removed_service = api_call_sync(query.get('server'), this.parentNode.parentNode.parentNode.id + '/' + query.get('id') + '/service','DELETE', {});
							if (removed_service.code != 200)
								return false;
							if (this.parentNode.parentNode.parentNode.id == 'optimus-structures')
							{
								parent.document.getElementById('associes').labels[0].style.display = 'none';
								parent.document.getElementById('associes').style.display = 'none';
							}
							this.parentNode.parentNode.parentNode.cells[1].innerHTML = '';
						}
						else 
							return false;
					else
					{
						newservice = api_call_sync(query.get('server'), this.parentNode.parentNode.parentNode.id + '/' + query.get('id') + '/service','POST', {})
						if (newservice.code != 201)
							return false;
						if (this.parentNode.parentNode.parentNode.id == 'optimus-structures')
						{
							parent.document.getElementById('associes').labels[0].style.display = '';
							parent.document.getElementById('associes').style.display = '';
						}
						this.parentNode.parentNode.parentNode.cells[1].innerHTML = 'V.' + newservice.data.version;
						
					}
				}
				label.appendChild(input);
				span = document.createElement('span');
				span.classList.add('slider');
				label.appendChild(span);
			}
			for (users_service of users_services.data)
			{
				document.getElementById(users_service.name).cells[1].innerHTML += 'V.'+users_service.version;
				document.getElementById(users_service.name).cells[2].firstChild.firstChild.checked = true;
			}
				

			document.getElementById('container').style.display = '';
		}


		
		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>