<!DOCTYPE html>
<html lang="fr">

<head>
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/editor.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/editor.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<div id="container" class="flex_container" style="max-width:926px;margin:auto;display:none"">

		<div class="editor" style="min-width:410px">
			<div>ASSOCIES DE LA STRUCTURE</div>
			<div style="text-align:center;line-height:18px">
				<table id="shares"></table>
				<button onclick="add_associe({})" style="margin-top:10px"><img src="/lib/fontawesome/plus.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Ajouter un utilisateur</button>
			</div>
		</div>

	</div>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		parent.window.document.title = 'ASSOCIES';
		users = {};
		
		api_call(query.get('server'),'allspark/logged','GET', {},'init');
		function init(response)
		{
			//document.getElementById('user').innerHTML = response.data.email;
			users = api_call_sync(query.get('server'),'allspark/users','GET', {});
			associes = api_call_sync(query.get('server'),'optimus-structures/'+query.get('id')+'/associes','GET', {});
			if (associes.data)
				for(associe of associes.data)
					add_associe(associe);
			document.getElementById('container').style.display = '';
		}

		function edited(response,object)
		{
			if (response.code == 200)
				object.classList.remove('editing');
		}

		function populate(object)
		{
			lastvalue = object.value;
			object.innerHTML = '';
			existing_shares = [];
			for (i=0; i<document.getElementById('shares').getElementsByTagName('select').length; i++)
				existing_shares[existing_shares.length] = document.getElementById('shares').getElementsByTagName('select')[i].value;
			for(user of users.data)
				if (user.id != query.get('id') && !existing_shares.includes(user.id))
					object.add(new Option(user.firstname && user.lastname ? user.firstname + ' ' + user.lastname : user.email, user.id));
			object.value = lastvalue;
			this.focus();
		}

		function add_associe(associe)
		{
			tr = document.getElementById('shares').insertRow();
			tr.id = associe.id;
			td1 = tr.insertCell();
			td1.style.textAlign = 'right';
			td1.style.paddingBottom = '32px';
			td1.innerHTML += 'Associé : ';
			select = document.createElement('select');
			td1.appendChild(select);
			select.id = 'user_'+associe.id;
			select.style.width = '250px';
			select.onclick = function(){populate(this)}
			select.onchange = function()
			{
				if (this.lastvalue == 0)
				{
					new_associe = api_call_sync(query.get('server'),'optimus-structures/'+query.get('id')+'/associes','POST', {'user':this.value});
					if (new_associe.code == 201)
						new_structure = api_call_sync(query.get('server'),'optimus-avocats/'+this.value+'/structures','POST', {'user':query.get('id')});
					this.parentNode.parentNode.id = new_associe.data.id;
					this.id = 'user_'+new_associe.data.id;
					this.structure = new_structure.data.id;
					this.classList.remove('editing')
				}
				else
				{
					api_call_sync(query.get('server'),'optimus-structures/'+query.get('id')+'/associes/'+this.parentNode.parentNode.id,'PATCH', {'user':this.value},'edited',this)
					structure = api_call_sync(query.get('server'),'optimus-avocats/'+this.lastvalue+'/structures','GET', {'user':query.get('id')});
					if (structure.code == 200)
						api_call_sync(query.get('server'),'optimus-avocats/'+this.lastvalue+'/structures/'+structure.data[0].id,'DELETE', {});
					api_call_sync(query.get('server'),'optimus-avocats/'+this.value+'/structures','POST', {'user':query.get('id'), 'entree':this.parentNode.getElementsByTagName('input')[0].value, 'sortie':this.parentNode.getElementsByTagName('input')[1].value});
				}
					
				this.lastvalue = this.value;
				
			}
			select.onfocus = function(){this.classList.add('editing')}
			select.onblur = function()
			{
				this.classList.remove('editing');
				if (this.value == 0)
					this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
			}
			select.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			select.oncontextmenu = function(){this.value=''; return false}
			if (!associe.id)
			{
				select.add(new Option('',0));
				populate(select);
				select.focus();
				select.lastvalue = 0;
			}
			else
				for(user of users.data)
					if (user.id == associe.user)
						select.add(new Option(user.firstname && user.lastname ? user.firstname + ' ' + user.lastname : user.email, user.id));
			select.value = associe.user || 0;
			select.lastvalue = associe.user || 0;

			br = document.createElement("br");
			td1.appendChild(br);
			text = document.createTextNode("Date d'entrée : ");
			td1.appendChild(text);
			entree = document.createElement('input');
			entree.style.width = '240px';
			entree.type = 'date';
			entree.value = associe.entree;
			entree.onblur = function ()
			{
				api_call(query.get('server'),'optimus-structures/'+query.get('id')+'/associes/'+this.parentNode.parentNode.id,'PATCH',{'entree':this.value},'edited',this);
				structure = api_call_sync(query.get('server'),'optimus-avocats/'+this.parentNode.getElementsByTagName('select')[0].value+'/structures','GET',{'user':query.get('id')});
				if (structure.code == 200)
					api_call(query.get('server'),'optimus-avocats/'+this.parentNode.getElementsByTagName('select')[0].value+'/structures/'+structure.data[0].id,'PATCH',{'entree':this.value});
			}
			entree.onfocus = function (){this.classList.add('editing')}
			entree.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			entree.oncontextmenu = function(){this.value='';return false}
			td1.appendChild(entree);

			br = document.createElement("br");
			td1.appendChild(br);
			text = document.createTextNode("Date de sortie : ");
			td1.appendChild(text);
			sortie = document.createElement('input');
			sortie.style.width = '240px';
			sortie.type = 'date';
			sortie.value = associe.sortie;
			sortie.onblur = function ()
			{
				api_call(query.get('server'),'optimus-structures/'+query.get('id')+'/associes/'+this.parentNode.parentNode.id,'PATCH', {'sortie':this.value},'edited',this);
				structure = api_call_sync(query.get('server'),'optimus-avocats/'+this.parentNode.getElementsByTagName('select')[0].value+'/structures','GET', {'user':query.get('id')});
				if (structure.code == 200)
					api_call(query.get('server'),'optimus-avocats/'+this.parentNode.getElementsByTagName('select')[0].value+'/structures/'+structure.data[0].id,'PATCH', {'sortie':this.value});
			}
			sortie.onfocus = function (){this.classList.add('editing')}
			sortie.onkeydown = function(){if (event&&event.keyCode==13) this.blur()}
			sortie.oncontextmenu = function(){this.value=''; return false}
			td1.appendChild(sortie);

			td2 = tr.insertCell();
			deleter = document.createElement('img');
			deleter.style.verticalAlign = 'middle';
			deleter.src = '/lib/fontawesome/trash.svg';
			deleter.style = 'height:18px;vertical-align:24px;margin-left:16px;filter:contrast(60%);cursor:pointer';
			deleter.onclick = function()
			{
				if (this.parentNode.firstChild.value==0)
					this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				else
				{
					associe = api_call_sync(query.get('server'),'optimus-structures/'+query.get('id')+'/associes/'+this.parentNode.parentNode.id,'DELETE', {});
					if (associe.code == 200)
					{
						structure = api_call_sync(query.get('server'),'optimus-avocats/'+this.parentNode.parentNode.firstChild.getElementsByTagName('select')[0].value+'/structures','GET', {'user':query.get('id')});
						api_call(query.get('server'),'optimus-avocats/'+this.parentNode.parentNode.firstChild.getElementsByTagName('select')[0].value+'/structures/'+structure.data[0].id,'DELETE', {});
					}	
					if (associe.code == 200 && structure.code == 200)
						this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
				}
			};
			td2.appendChild(deleter);
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}

	</script>
</body>
</html>