<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>INTERVENTIONS</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/interventions/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/dynamictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/dynamictable.js"></script>
	<script src="/js/number_format.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		
		dynamictable = new Object();
		dynamictable.server = query.get('server');
		dynamictable.api = 'optimus-avocats';
		dynamictable.resource = 'dossiers_interventions';
		dynamictable.owner = query.get('owner');
		dynamictable.user = get_cookie('user_id');
		dynamictable.grid = <?php include_once('dossiers_interventions.json');?>;
		dynamictable.advanced_search = [];
		dynamictable.global_search = "";
		dynamictable.page = 1;
		dynamictable.results = 30;
		dynamictable.version = 45.6;
		
		api_call(query.get('server'),'allspark/logged', 'GET', {}, 'init');
		
		function init(response)
		{
			dynamictable_init(dynamictable);
		}
		
		function dossier(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function()
			{
				editor = window.open('/modules/dossiers/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+row[1][0],'editor');
			}
			return row[column];
		}
		
		function intervention(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function()
			{
				editor = window.open('/modules/interventions/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&dossier='+row[1][0]+'&id='+row[0],'editor');
			}
			return 'FICHE N°' + row[column];
		}
		
		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
	</script>
</body>
</html>