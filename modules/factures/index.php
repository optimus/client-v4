<!DOCTYPE html>
<html lang="fr">

<head>
	<title>FACTURES</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/factures/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/dynamictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/dynamictable.js"></script>
	<script src="/js/number_format.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>
	
	<select id="structures" style="display:none;position:fixed;left:0;top:0;width:auto" onchange="dynamictable.owner = this.value.split('`')[1];dynamictable_update()"></select>
	
	<script type="text/javascript">
		
		const query = new URLSearchParams(window.location.search);
		
		dynamictable = new Object();
		dynamictable.server = query.get('server');
		dynamictable.api = 'optimus-structures';
		dynamictable.resource = 'factures';
		dynamictable.owner = query.get('owner');
		dynamictable.user = get_cookie('user_id');
		dynamictable.grid = <?php include_once('factures.json');?>;
		dynamictable.advanced_search = [];
		dynamictable.global_search = "";
		dynamictable.page = 1;
		dynamictable.results = 30;
		dynamictable.version = 46;
		
		servers = {};

		api_call(query.get('server'),'allspark/preferences', 'GET', {'owner':query.get('owner'), 'user': get_cookie('user_id'),'module':dynamictable.resource}, 'init');
		
		function init(response)
		{
			structures = api_call_sync(query.get('server'),'optimus-avocats/' +get_cookie('user_id') + '/structures','GET',{});
			for (structure of structures.data)
			{
				user = api_call_sync(structure.server,'allspark/users/' + structure.user,'GET',{});
				document.getElementById('structures').options[document.getElementById('structures').options.length] = new Option([user.data.firstname,user.data.lastname].filter(Boolean).join(' '), structure.server+'`'+structure.user+'`'+structure.id);
			}
				
			for (structure of structures.data)
				if (!structure.sortie)
					break;
			document.getElementById('structures').value = structure.server+'`'+structure.user+'`'+structure.id;
			if (document.getElementById('structures').options.length > 1)
				document.getElementById('structures').style.display = '';
			
			dynamictable.server = structure.server;
			dynamictable.owner = structure.user;
			dynamictable.user = structure.user;
			
			dynamictable_init(dynamictable);

			
			//for (facture of data)
				//servers[facture[1]] = {};
			//for (facture of data)
				//servers[facture[1]][facture[2]] = {};
			//for (facture of data)
				//servers[facture[1]][facture[2]].dossiers = api_call_sync(facture[1],'optimus-avocats/' + facture[2] + '/dossiers','GET',{'fields':['id','nom']}).data;
			
			preset = document.createElement('button');
			preset.innerHTML = 'FACTURES IMPAYEES';
			preset.onclick = function()
			{
				dynamictable.advanced_search = [{"field":"11","operator":"<","value":"100","next":"AND"},{"field":"2","operator":"==","value":"dev@devoptimus.ovh","next":"AND"},{"field":"12","operator":"==","value":"0","next":"AND"}];
				dynamictable.sort = [8];
				dynamictable.page = 1;
				dynamictable_update();
			}
			document.getElementById('controls').appendChild(preset);
			
		}
	
		function ratio(row,column)
		{
			if(row[column][0]==null)
				return 0;
			else if(row[column][1]<100)
				td.style.color='#FF0000';
			else
				td.style.color='#008000';
			return row[column][1];
		}
		
		function facture(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function(){editor = window.open('/modules/factures/editor.php?server='+row[1]+'&owner='+dynamictable.owner+'&id='+row[0],'editor')}
			return row[column];
		}
		
		function client(row,column)
		{
			if (row[4][0] > 0)
			{
				td.style.cursor='pointer';
				td.style.color='#0000B0';
				td.onclick=function(){editor = window.open('/modules/contacts/editor.php?server='+row[1]+'&owner='+row[2]+'&id='+row[4][0],'editor')}
			}
				
			if (row[1] == document.getElementById('structures').value.split('`')[0])
				return row[column];
			else
				return '<i>(nom indisponible)</i>';
		}
		
		function dossier(row,column)
		{
			if (row[4][0] > 0)
			{
				td.style.cursor='pointer';
				td.style.color='#0000B0';
				td.onclick=function(){editor = window.open('/modules/dossiers/editor.php?server='+row[1]+'&owner='+row[2]+'&id='+row[5][0],'editor')}
			}

			if (row[1] == document.getElementById('structures').value.split('`')[0])
				return row[column];
			else
				return '<i> (nom indisponible)</i>';
		}
		
		function intervention(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function()
			{
				editor = window.open('/modules/interventions/editor.php?server='+row[1]+'&owner='+row[2]+'&dossier='+row[5][0]+'&id='+row[6],'editor');
			}
			return 'FICHE N°' + row[column];
		}
		
		
		
		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
	</script>
</body>
</html>