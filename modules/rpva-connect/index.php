<!doctype html>
<html>
	<head>
		<title>RPVA CONNECT</title>
		<link rel="shortcut icon" type="image/png" href="/modules/rpva-connect/icon.svg">
		<link href="/css/main.css" rel="stylesheet" type="text/css">
		<link href="/css/editor.css" rel="stylesheet" type="text/css">
		<link href="/css/statictable.css" rel="stylesheet" type="text/css">
		<script src="/js/api_call.js"></script>
		<script src="/js/main.js"></script>
		<script src="/js/editor.js"></script>
		<script src="/js/number_format.js"></script>
		<script src="/js/cookies.js"></script>
	</head>

	<style>
	.statictable tbody tr:hover
	{
		background-color: #FF8000;
	}
	</style>
	
	<body>

		<table align="left" cellpadding="20" style="position:absolute;left:0;top:0">
			<tr>
				<td>
					STATUS<br/>
					<select id="status" multiple style="overflow:hidden;width:100px" onchange="refresh()">
						<option selected>OUVERT</option>
						<option>FERMÉ</option>
					</select>
				
				<br/><br/>

					TYPE DE JURIDICTION<br/>
					<select id="type_juridiction" multiple style="overflow:hidden;width:200px" onchange="refresh()">
						<option selected value="TGI">Tribunaux de grande Instance</option>
						<option selected value="CA">Cours d'appel</option>
					</select>
				
				<br/><br/>

					TGI<br/>
					<select id="tgi" multiple style="overflow:hidden;width:200px" onchange="document.getElementById('type_juridiction').value='TGI';document.getElementById('ca').value=0;refresh()">
						
					</select>
				
				<br/><br/>
				
					CA<br/>
					<select id="ca" multiple style="overflow:hidden;width:200px" onchange="document.getElementById('type_juridiction').value='CA';document.getElementById('tgi').value=0;refresh()">
						
					</select>
				</td>
			</tr>
		</table>

		<div style="font-size:32px;margin-top:30px">RPVA CONNECT</div>
		<div style="font-size:18px"><i>Preuve de concept</i></div>
		<div id="firefox_instructions" style="display:none">
			Pour activer le RPVA dans Optimus, connectez-vous au RPVA2 avec votre clé ou via OTP : <a href="https://backend.ebarreau2.fr/saml/login" target="_blank" style="color:#0000FF">https://backend.ebarreau2.fr/saml/login</a></br>
			Puis cliquez sur : <button onclick="window.location.reload()">Recharger la page</button><br/>
		</div>
		<div id="other_instructions" style="display:none;color:#FF0000">
			<br/>
			En raisons de restrictions imposées par le CNB<br/>
			RPVA CONNECT ne fonctionne qu'avec FIREFOX<br/>
			Signez <a href="https://www.change.org/p/conseil-national-des-barreaux-un-acc%C3%A8s-individuel-%C3%A0-l-api-rpva-pour-chaque-avocat" style="color:#0000FF">la pétition</a> pour que le CNB permette un accès complet.<br/>
			<br/>
		</div>
		<br/>
		Recherche<br/>
		<input id="name_filter" onchange="refresh()" onkeyup="if (event && event.keyCode == 13" autofocus/>

		<table class="statictable" align="center" style="margin-top:20px">
			<thead>    
				<tr>
					<td align="center">ID</td>
					<td align="center">STATUS</td>
					<td align="center">TYPE</td>
					<td align="center">NOM</td>
					<td align="center">JURIDICTION</td>
					<td align="center">RG</td>
				</tr>
			</thead>
			<tbody id="dossiers"></tbody>
		</table>

	</body>

	<script>	
		const query = new URLSearchParams(window.location.search);

		if (detect_browser()=='Firefox')
			document.getElementById('firefox_instructions').style.display = '';
		else
			document.getElementById('other_instructions').style.display = '';

		function refresh()
		{
			
			while(document.getElementById('dossiers').rows.length > 0)
				document.getElementById('dossiers').deleteRow(0);

			if (document.getElementById('type_juridiction').options[0].selected)
			{
				endpoint  = 'https://backend.ebarreau2.fr/affaireProcedures?';
				endpoint += 'legalCaseStatus='+Array(...document.getElementById('status').querySelectorAll("option:checked")).map(option => option.value);
				endpoint += '&juridiction=TGI';
				if (document.getElementById('type_juridiction').options[0].selected)
					endpoint += '&tgi='+Array(...document.getElementById('tgi').querySelectorAll("option:checked")).map(option => option.value);
				else
					endpoint += '&tgi=';
				endpoint += '&typeDossier=&sortField=lastModifiedDate-desc&page=1&parPage=1000';

				fetch(endpoint,{credentials: "include"})
				.then(response => response.json())
				.then(function(response)
				{
					if (response.code === 401 && response.message == 'Accès refusé')
						alert('Accès refusé');
					else if (response.code >= 400)
						alert("ERREUR " + response.code);

					response.elements.forEach(display_rows);
				});
			}
			
			if (document.getElementById('type_juridiction').options[1].selected)
			{
				endpoint  = 'https://backend.ebarreau2.fr/affaireProcedures?';
				endpoint += 'legalCaseStatus='+Array(...document.getElementById('status').querySelectorAll("option:checked")).map(option => option.value);
				endpoint += '&juridiction=CA';
				if (document.getElementById('type_juridiction').options[1].selected)
					endpoint += '&ca='+Array(...document.getElementById('ca').querySelectorAll("option:checked")).map(option => option.value);
				else
					endpoint += '&ca=';
				endpoint += '&typeDossier=&sortField=lastModifiedDate-desc&page=1&parPage=1000';

				fetch(endpoint,{credentials: "include"})
				.then(response => response.json())
				.then(function(response)
				{
					if (response.code === 401 && response.message == 'Accès refusé')
						alert('Accès refusé');
					else if (response.code >= 400)
						alert("ERREUR " + response.code);

					response.elements.forEach(display_rows);
				});
			}
		}

			function populate_tgi()
			{
				fetch('https://backend.ebarreau2.fr/search/juridiction?tribunalNom=',{credentials: "include"})
				.then(response => response.json())
				.then(function(response)
				{
					for(juridiction of response)
						document.getElementById('tgi').add(new Option(juridiction.name,juridiction.name,false, true));
					if (detect_browser()=='Firefox')
						document.getElementById('tgi').style.height = document.getElementById('tgi').options.length * 19 + 'px';
					else
						document.getElementById('tgi').style.height = document.getElementById('tgi').options.length * 17 + 'px';
				});
			}

			function populate_ca()
			{
				fetch('https://backend.ebarreau2.fr/listCA',{credentials: "include"})
				.then(response => response.json())
				.then(function(response)
				{
					for(juridiction of response)
						if (juridiction.id != 36 && juridiction.id != 37)
							document.getElementById('ca').add(new Option(juridiction.name,juridiction.name,false,true));
					if (detect_browser()=='Firefox')
						document.getElementById('ca').style.height = document.getElementById('ca').options.length * 18 + 24 + 'px';
					else
						document.getElementById('ca').style.height = document.getElementById('ca').options.length * 16 + 24 + 'px';
				});
			}

			function display_rows(dossier)
			{
				if (document.getElementById('name_filter').value.length > 0 && dossier.name.toLowerCase().search(document.getElementById('name_filter').value.toLowerCase()) == -1)
					return true;
				tr = document.getElementById('dossiers').insertRow();
				tr.onclick = function(){window.open('https://ebarreau2.fr/#/lawyer/affaires/'+dossier.id+'/')};
				tr.style.cursor = 'pointer';
				td0 = tr.insertCell();
				td0.innerHTML = dossier.id;
				td0.style.textAlign = 'left';
				td1 = tr.insertCell();
				td1.innerHTML = dossier.status;
				td2 = tr.insertCell();
				td2.innerHTML = dossier.caseType;
				td3 = tr.insertCell();
				td3.style.textAlign = 'left';
				if (dossier.name)
					td3.innerHTML = dossier.name.split('_')[0];
				td4 = tr.insertCell();
				td4.style.textAlign = 'left';
				if (dossier.procedures[0].tribunalType)
					td4.innerHTML += dossier.procedures[0].tribunalType;
				if (dossier.procedures[0].tgi)
					if (dossier.procedures[0].tgi.name)
						td4.innerHTML += ' ' + dossier.procedures[0].tgi.name;
				if (dossier.procedures[0].ca)
					if (dossier.procedures[0].ca.name)
						td4.innerHTML += ' ' + dossier.procedures[0].ca.name;
				td5 = tr.insertCell();
				td5.style.align = 'left';
				if (dossier.name)
					if (dossier.name.split('_')[1])
					{
						if (dossier.name.split('_')[1].length > 8)
							td5.innerHTML = dossier.name.split('_')[1].substring(2);
						else
							td5.innerHTML = dossier.name.split('_')[1];
					}
			}

			populate_tgi();
			populate_ca();
			if (detect_browser()=='Firefox')
				document.getElementById('type_juridiction').style.height = document.getElementById('type_juridiction').options.length * 21 + 'px';
			else
				document.getElementById('type_juridiction').style.height = document.getElementById('type_juridiction').options.length * 17 + 'px';
			
			if (detect_browser()=='Firefox')
				document.getElementById('status').style.height = document.getElementById('status').options.length * 21 + 'px';
			else
				document.getElementById('status').style.height = document.getElementById('status').options.length * 17 + 'px';
			refresh();
	</script>
</html>