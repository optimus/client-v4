<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/statictable.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
	</head>
	
	<body style="display:none">

		<table id="interventions" class="statictable" align="center" cellpadding="3" cellspacing="0" border="1" bordercolor="000000" style="border-collapse:collapse;margin-top:15px;margin-bottom:15px">
			<thead>
				<tr height="16" style="background-color:#5E79B0;color:#FFFFFF;font-weight:bold">
					<td align="center" width="24" style="border:1px solid #000000;cursor:pointer" oncontextmenu="for (i=0; i<document.getElementById('interventions').tBodies[0].rows.length; i++) document.getElementById('interventions').tBodies[0].rows[i].cells[0].lastChild.checked=false;return false;" onclick="for (i=0; i<document.getElementById('interventions').tBodies[0].rows.length; i++) document.getElementById('interventions').tBodies[0].rows[i].cells[0].lastChild.checked=true">&nbsp;</td>
					<td align="center" width="100">FICHE</td>
					<td align="center" width="82">CREEE LE</td>
					<td align="center" width="358">DESCRIPTION</td>
					<td align="center" width="200">FACTURE</td>
					<td align="center" width="24">&nbsp;</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		
		<div style="text-align:center">
			<button onclick="intervention_create()">AJOUTER UNE FICHE D'INTERVENTION</button>
		</div>
	
	</body>

	<script language="JavaScript" type="text/javascript">	
		const query = new URLSearchParams(window.location.search);
			
		api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/interventions','GET', {},'init');
		function init(response)
		{
			for (intervention of response.data)
				display_intervention(intervention.id, intervention.date_ouverture,intervention.description, intervention.server, intervention.structure, intervention.facture);
			document.body.style.display = 'inline';
		}
		
		function intervention_create()
		{
			structures = api_call_sync(query.get('server'),'optimus-avocats/' + query.get('owner') + '/structures','GET',{});
			for (structure of structures.data)
				if (!structure.sortie)
					break;

			new_intervention = api_call_sync(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/interventions','POST', {'dossier':query.get('id'),'server':structure.server,'structure':structure.user});
			display_intervention(new_intervention.data.id, new_intervention.data.date_ouverture,'','non facturé');
		}
		
		var x=0; 
		function display_intervention(id, date_ouverture, description, server, owner, facture_id)
		{
			var tr = document.createElement('TR'); 
			tr.style.height = '16px'; 
			tr.style.font = '12px Roboto'; 
			tr.id = id; 
			x +=1; if (x % 2 == 0) tr.style.backgroundColor="#EBF1FF"; else tr.style.backgroundColor="#FFFFFF";
			document.getElementById('interventions').tBodies[0].appendChild(tr);
			
			var td0 = document.createElement('TD');
			td0.style.border = '1px solid #000000';
			td0.onclick = function(event){if (event.srcElement.type!='checkbox') this.lastChild.click();}
			td0.innerHTML = '<input type="checkbox" />';
			tr.appendChild(td0);
			
			var td1 = document.createElement('TD'); 
			td1.style.textAlign = "left";
			td1.style.color = '#0000FF';
			td1.style.cursor = "pointer";
			td1.innerHTML = "FICHE N°" + id;
			td1.onclick=function(){window.open('/modules/interventions/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&dossier='+query.get('id')+'&id='+id,'diligences')};
			tr.appendChild(td1);
			
			var td2 = document.createElement('TD');
			td2.style.textAlign = "center";
			input = document.createElement('input');
			input.style = 'width:71px;height:13px;border:0px;font:normal 12px Roboto;background:transparent;outline:0;text-align:center';
			input.value = date_ouverture;
			input.onkeydown = function() {if (event&&event.keyCode==13)this.blur()}
			input.onfocus = function(){this.style.color='#FF0000'}
			input.onblur = function()
			{
				if(api_call_sync(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/interventions/'+this.parentNode.parentNode.id,'PATCH',{"date_ouverture":this.value}).code == 200)
					this.style.color='#000000';
			}
			td2.appendChild(input);
			tr.appendChild(td2);
			
			var td3 = document.createElement('TD');
			td3.style.textAlign = "left";
			input = document.createElement('input');
			input.style = 'width:350px;height:13px;border:0px;font:normal 12px Roboto;background:transparent;outline:0';
			input.value = description || '';
			input.onkeydown = function() {if (event&&event.keyCode==13)this.blur()}
			input.onfocus = function(){this.style.color='#FF0000'}
			input.onblur = function()
			{
				if(api_call_sync(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/interventions/'+this.parentNode.parentNode.id,'PATCH',{"description":this.value}).code == 200) 
					this.style.color='#000000';
			}
			td3.appendChild(input);
			tr.appendChild(td3);
			
			var td4 = document.createElement('TD');
			td4.style.textAlign = "center";
			
			if (facture_id)
			{
				facture = api_call_sync(server,'optimus-structures/'+owner+'/factures/'+facture_id,'GET',{});
				if (facture.code == 200)
				{
					td4.innerHTML = facture.data[0].numero + '<br/>' + facture.data[0].db;
					td4.style.cursor = "pointer";
					td4.onclick = function(){parent.location='/modules/factures/editor.php?server='+server+'&owner='+owner+'&id='+facture_id};
				}
			}
			else
				td4.innerHTML = 'non facturé<br/><br/>';
			tr.appendChild(td4);
			
			var td5 = document.createElement('TD');
			td5.style.textAlign = "center";
			td5.innerHTML = '<img src="/lib/fontawesome/trash.svg" style="width:14px;filter:contrast(60%)">';
			td5.style.cursor = 'pointer';
			td5.onclick = function()
			{
				if(confirm('Etes vous sûr ?\nToutes les diligences saisies dans cette fiche seront perdues !')) 
					if(api_call_sync(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/interventions/'+this.parentNode.id,'DELETE',{}).code == 200) 
						document.getElementById("interventions").deleteRow(this.parentNode.rowIndex);
			}
			tr.appendChild(td5);
		}	
		


	// window.addEventListener('copy',copy);
	// window.addEventListener('cut',cut);
	// window.addEventListener('paste',paste);
	
	// function reset()
	// {
		// for (i=document.getElementById('interventions').tBodies[0].rows.length-1; i>=0 ; i--)
		// {
			// document.getElementById('interventions').tBodies[0].rows[i].style.webkitFilter = 'blur(0px)';
			// if (i%2==0)
				// document.getElementById('interventions').tBodies[0].rows[i].style.background = '#FFFFFF';
			// else
				// document.getElementById('interventions').tBodies[0].rows[i].style.background = '#EBF1FF';
		// }
	// }
	
	// function cut()
	// {
		// reset();
		// array = new Array();
		// array[array.length] = '<?php echo $_GET['db']?>';
		// array[array.length] = '<?php echo $_GET['id']?>';
		// for (i=document.getElementById('interventions').tBodies[0].rows.length-1; i>=0; i--)
			// if (document.getElementById('interventions').tBodies[0].rows[i].cells[0].lastChild.checked==true)
				// if (document.getElementById('interventions').tBodies[0].rows[i].cells[4].innerHTML=='non facturé')
				// {
					// array[array.length] = document.getElementById('interventions').tBodies[0].rows[i].id;
					// document.getElementById('interventions').tBodies[0].rows[i].style.webkitFilter = 'blur(3px)';
				// }
				
		// if (array.length>2)
			// clipboard("optimus_interventions_cut"+JSON.stringify(array));
	// }
	
	// function copy()
	// {
		// reset();
		// array = new Array();
		// array[array.length] = '<?php echo $_GET['db']?>';
		// array[array.length] = '<?php echo $_GET['id']?>';
		// for (i=0; i<document.getElementById('interventions').tBodies[0].rows.length; i++)
			// if (document.getElementById('interventions').tBodies[0].rows[i].cells[0].lastChild.checked==true)
			// {
				// array[array.length] = document.getElementById('interventions').tBodies[0].rows[i].id;
				// document.getElementById('interventions').tBodies[0].rows[i].style.background = '#FF8000';
			// }
				
		// if (array.length>2)
			// clipboard("optimus_interventions_copy"+JSON.stringify(array));
	// }
	
	// function paste(e) 
	// {
		// paste = e.clipboardData.getData('Text');
		// if (paste.substring(0,26) == 'optimus_interventions_copy')
		// {
			// array = eval(paste.substring(26));
			// for (i=2; i<array.length; i++)
			// {
				// intervention = eval(db_query('*',db,'interventions','id','=',array[i]));
				// new_id = db_insert(db,'interventions',['date_ouverture','description','dossier'],['<?php echo date('Y-m-d')?>',intervention[0]['description'],'<?php echo $_GET['id']?>']);
				// display_intervention(new_id, new Date().toISOString().substring(8,10) + '/' + new Date().toISOString().substring(5,7) + '/' + new Date().toISOString().substring(0,4), intervention[0]['description'], 'non facturé', 0, db);
				// diligences = eval(db_query('*',db,'interventions_diligences','intervention','=',array[i]));
				// if (diligences)
					// for(j=0;j<diligences.length;j++)
						// db_insert(db,'interventions_diligences',['intervention','date','intervenant','compte','commission','description','categorie','type','coefficient','tarif'],[new_id,diligences[j]['date'],diligences[j]['intervenant'],diligences[j]['compte'],diligences[j]['commission'],diligences[j]['description'],diligences[j]['categorie'],diligences[j]['type'],diligences[j]['coefficient'],diligences[j]['tarif']]);
			// }
		// }
		// if (paste.substring(0,25) == 'optimus_interventions_cut')
		// {
			// array = eval(paste.substring(25));
			// for (k=2; k<array.length; k++)
				// if(array[0]=='<?php echo $_GET['db']?>')
				// {
					// db_update('<?php echo $_GET['db']?>','interventions','dossier','<?php echo $_GET['id']?>','id','=',array[k]);
					// intervention = eval(db_query('*',db,'interventions','id','=',array[k]));
					// if (intervention && !document.getElementById(intervention[0]['id']))
						// display_intervention(intervention[0]['id'], intervention[0]['date_ouverture'].substring(8,10) + '/' + intervention[0]['date_ouverture'].substring(5,7) + '/' + intervention[0]['date_ouverture'].substring(0,4), intervention[0]['description'], 'non facturé',0, '');
				// }
				// else
				// {
					// intervention = eval(db_query('*',array[0],'interventions','id','=',array[k]));
					// if (intervention)
					// {
						// new_id = db_insert('<?php echo $_GET['db']?>','interventions',['dossier','date_ouverture','description','honoraires','frais','debours'],['<?php echo $_GET['id']?>',intervention[0]['date_ouverture'],intervention[0]['description'],intervention[0]['honoraires'],intervention[0]['frais'],intervention[0]['debours']]);
						// display_intervention(new_id,intervention[0]['date_ouverture'].substring(8,10) + '/' + intervention[0]['date_ouverture'].substring(5,7) + '/' + intervention[0]['date_ouverture'].substring(0,4), intervention[0]['description'], 'non facturé',0, '');
						// db_delete(array[0],'interventions','id','=',array[k]);
						
						// diligences = eval(db_query('*',array[0],'interventions_diligences','intervention','=',array[k]));
						// if (diligences)
							// for(j=0;j<diligences.length;j++)
								// db_insert('<?php echo $_GET['db']?>','interventions_diligences',['intervention','date','intervenant','compte','commission','description','categorie','type','coefficient','tarif'],[new_id,diligences[j]['date'],diligences[j]['intervenant'],diligences[j]['compte'],diligences[j]['commission'],diligences[j]['description'],diligences[j]['categorie'],diligences[j]['type'],diligences[j]['coefficient'],diligences[j]['tarif']]);
						// db_delete(array[0],'interventions_diligences','intervention','=',array[k]);
					// }
				// }
		// }
		// reset();
	// }
	
	// function clipboard(data)
	// {
		// area = document.createElement("textarea");
		// document.body.appendChild(area);
		// area.value = data;
		// area.style.opacity=0;
 		// area.focus();
 		// area.select();
  	// document.execCommand("copy", false, null);
	// }
	</script>
</html>