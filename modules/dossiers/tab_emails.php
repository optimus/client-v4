<!DOCTYPE html>
<html>
	<head>
		<title>WEBMAIL</title>
		<meta charset="UTF-8">
		<script src="/js/api_call.js"></script>
		<script src="/js/utf7_encode.js"></script>
	</head>
	
	<script>
	const query = new URLSearchParams(window.location.search);
	api_call(query.get('server'), 'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id'), 'GET', {}, 'init');
	
	function init(response)
	{
		window.location.href = 'https://webmail.'+query.get('server')+'/?_task=mail&_action=list&_mbox=%3D%3DDOSSIERS%3D%3D%2F' + encodeURIComponent(encode_imap_utf7(response.data[0].nom));
	}
	</script>
</html>