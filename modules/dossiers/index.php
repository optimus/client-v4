<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>DOSSIERS</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/dossiers/icon.svg">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/dynamictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/dynamictable.js"></script>
	<script src="/js/number_format.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<select id="authorized_users" style="display:none;position:fixed;left:0;top:0;width:auto" onchange="window.location = window.location.href.replace('owner='+query.get('owner'), 'owner='+this.value)"></select>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		
		dynamictable = new Object();
		dynamictable.server = query.get('server');
		dynamictable.api = 'optimus-avocats';
		dynamictable.resource = 'dossiers';
		dynamictable.owner = query.get('owner');
		dynamictable.user = get_cookie('user_id');
		dynamictable.grid = <?php include_once('dossiers.json');?>;
		dynamictable.advanced_search = [];
		dynamictable.global_search = "";
		dynamictable.page = 1;
		dynamictable.results = 30;
		dynamictable.version = 47;
		
		api_call(query.get('server'),'allspark/logged', 'GET', {}, 'init');
		
		function init(response)
		{
			authorized_users = api_call_sync(query.get('server'),'allspark/authorizations','GET',{"resource":"*","user":get_cookie('user_id')}).data;
			for (authorized_user of authorized_users)
				if (authorized_user.read ==1)
					document.getElementById('authorized_users').options[document.getElementById('authorized_users').options.length] = new Option([authorized_user.firstname,authorized_user.lastname].filter(Boolean).join(' '), authorized_user.owner);
			if (document.getElementById('authorized_users').options.length > 1)
				document.getElementById('authorized_users').style.display = '';
			document.getElementById('authorized_users').value = query.get('owner');

			services = api_call_sync(query.get('server'),'allspark/users/'+query.get('owner')+'/services','GET',{}).data;
			if (!services.find(obj => obj.name == 'optimus-avocats'))
			{
				document.body.innerHTML += 'Cet utilisateur n\'a pas installé le service "optimus-avocats"';
				return false;
			}

			dynamictable_init(dynamictable);

			user_authorizations = authorized_users.find(obj => obj.owner == query.get('owner'));
			if (user_authorizations.create == 1)
			{
				insert = document.createElement('button');
				insert.innerHTML = 'NOUVEAU DOSSIER';
				insert.onclick = function(){api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers', 'POST', {}, 'dossier_created');}
				document.getElementById('controls').appendChild(insert);
			}
		}
		
		function dossier_created(dossier)
		{
			refresher = document.createElement('iframe');
			refresher.src = 'https://webmail.'+query.get('server')+'/?_task=settings&_action=folders';
			refresher.style.display = 'none';
			document.body.appendChild(refresher);
			editor = window.open('/modules/dossiers/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+dossier.data.id,'editor');
			setTimeout(function(){window.location.reload()},500);
		}
		
		function dossier(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function()
			{
				editor = window.open('/modules/dossiers/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+row[0],'editor');
			}
			return row[column];
		}
		
		function folder(row,column)
		{
			td.style.cursor='pointer';
			td.innerHTML = '<img src="/modules/files/icon.svg" style="width:16px"/>';
			td.onclick=function()
			{
				console.log(row);
				window.open('/modules/dossiers/tab_fichiers.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+ row[0])
			}
			return '<img src="/modules/files/icon.svg" style="width:16px;filter:drop-shadow(1px 1px black)"/>';
		}
		
		function emails(row,column)
		{
			td.style.cursor='pointer';
			td.innerHTML = 'link';
			td.onclick=function()
			{
				console.log(row);
				window.open('/modules/dossiers/tab_emails.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+ row[0])
			}
			return '<img src="/modules/webmail/icon.svg" style="width:16px;filter:drop-shadow(1px 1px black)"/>';
		}
		
		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
	</script>
</body>
</html>