<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta name="viewport" content="width=410, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="/modules/dossiers/icon.svg" />
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/tabs.css">
		<script src="/js/main.js"></script>
	</head>

	<body style="text-align:center;overflow-y:hidden" onload="load_tab()">
		<div class="tabs" onclick="if (event.target.name=='tabs') document.getElementById('0').checked = false">
			<input id="0" type="checkbox"/><label for="0"></label>
			<input id="1" type="radio" name="tabs" onclick="load_tab()" link="tab_general.php" checked/><label for="1">GENERAL</label>
			<input id="2" type="radio" name="tabs" onclick="load_tab()" link="tab_intervenants.php"/><label for="2">INTERVENANTS</label>
			<input id="3" type="radio" name="tabs" onclick="load_tab()" link="tab_notes.php"><label for="3">NOTES</label>
			<input id="4" type="radio" name="tabs" onclick="load_tab()" link="tab_interventions.php"><label for="4">INTERVENTIONS</label>
			<input id="5" type="radio" name="tabs" onclick="load_tab()" link="tab_factures.php"><label for="5">FACTURES</label>
			<input id="6" type="radio" name="tabs" onclick="load_tab()" link="tab_fichiers.php"><label for="6">FICHIERS</label>
			<input id="7" type="radio" name="tabs" onclick="load_tab()" link="tab_emails.php"><label for="7">EMAILS</label>
			<input id="8" type="radio" name="tabs" onclick="load_tab()" link="tab_fax.php"><label for="8">FAXS</label>
			<input id="9" type="radio" name="tabs" onclick="load_tab()" link="tab_agenda.php"><label for="9">AGENDA</label>	
			<input id="10" type="radio" name="tabs" onclick="load_tab()" link="tab_dictees.php"><label for="10">DICTÉES</label>
		</div>
		<div class="tab_separator"></div>
		<iframe id="frame" marginwidth="0" marginheight="0" frameborder="0" onload="this.style.height = (window.innerHeight - 57) + 'px'" style="width:100%"></iframe>
	</body>
</html>