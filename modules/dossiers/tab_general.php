<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/editor.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
		<script src="/js/editor.js"></script>
	</head>

	<body>

	<div class="flex_container" style="max-width:926px;margin:auto">

		<div class="editor" style="min-width:410px">
			<div>DOSSIER</div>
			<div>
				Nom : <input id="nom" type="text" tabindex="1" style="width:274px" onblur="dossier_rename(this)" onkeyup="filename(this)"/><br/>
				Numéro : <input id="numero" type="text" maxlength="8" tabindex="2" style="width:274px" onblur="simple_save(this)"/><br/>
				<br/>
				Ouverture : <input id="date_ouverture" type="date" tabindex="3" onblur="simple_save(this)"style="width:274px"/><br/>
				Classement : <input id="date_classement" type="date" tabindex="4" onblur="simple_save(this)" style="width:274px"/><br/>
				N° Archive : <input id="numero_archive" type="number" type="text" tabindex="5" onblur="simple_save(this)" style="width:274px"/><br/>
				<br/>
				Domaine : <select tabindex="6" id="domaine" onchange="simple_save(this);populate(document.getElementById('sous_domaine'),'dossiers_sousdomaines',true, true, 0, this.value);simple_save(document.getElementById('sous_domaine'))" onblur="this.classList.remove('editing')" style="width:284px"/></select><br/>
				Matière : <select tabindex="7" id="sous_domaine" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:284px"/></select><br/>
				<br/>
				N° RG : <input id="rg" type="text" tabindex="8" onblur="simple_save(this)" style="width:274px"/><br/>
				N° Portalis : <input id="portalis" type="text" tabindex="9" onblur="simple_save(this)" style="width:274px"/><br/>
				N° Parquet : <input id="parquet" type="text" tabindex="9" onblur="simple_save(this)" style="width:274px"/><br/>
				N° Instruction : <input id="instruction" type="text" tabindex="9" onblur="simple_save(this)" style="width:274px"/><br/>
				<br/>
				Conseil : <input id="conseil" type="checkbox" tabindex="10" onclick="simple_save(this)" style="margin-right:40px"/>
				Contentieux : <input id="contentieux" type="checkbox" tabindex="11" onclick="simple_save(this)" style="margin-right:50px"/>
				A.J. : <input id="aj" type="checkbox" tabindex="12" onclick="simple_save(this)" style="margin-right:26px"/><br/>
				<br/>
				<div style="text-align:center">
					<button onclick="if(confirm('Etes vous sûr de vouloir supprimer ce dossier ?\nCela entrainera la suppression des fichiers et emails associés')) dossier_delete();"><img src="/lib/fontawesome/trash.svg" style="height:16px;opacity:0.6;vertical-align:-4px"/>&nbsp;&nbsp;<span style="color:#FF0000">Supprimer</span></button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button onclick="parent.window.open('about:blank', '_self');parent.window.close()"><img src="/lib/fontawesome/times.svg" style="width:14px;filter:contrast(60%);vertical-align:-6px">&nbsp;&nbsp;Quitter</button>
				</div>
			</div>
		</div>

		<script type="text/javascript">

			const query = new URLSearchParams(window.location.search);
			api = 'optimus-avocats';
			resource = 'dossiers';
			itm = {};
			authorizations = {};
			
			api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id'),'GET', {},'init');
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				populate(document.getElementById('domaine'),'dossiers_domaines',true, true);
				editor_init();
				populate(document.getElementById('sous_domaine'),'dossiers_sousdomaines',true, true, itm.sous_domaine, itm.domaine);
				window.old_name = itm.nom;
				parent.window.document.title = itm.nom;
				document.body.style.display = 'inline';
			}
			
			function dossier_delete()
			{
				api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+itm.id,'DELETE',{}, 'dossier_deleted');
			}

			function dossier_deleted(response)
			{
				if (response.code == 200)
				{
					refresher = document.createElement('iframe');
					refresher.src = 'https://webmail.' + query.get('server') + '/?_task=settings&_action=folders';
					refresher.style.display = 'none';
					document.body.appendChild(refresher);
					setTimeout(function(){parent.window.open('about:blank', '_self');parent.window.close()},500);
				}
			}

			function dossier_rename(obj)
			{
				if (obj.value != itm.nom)
				{
					api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+itm.id,'PATCH',{"nom": obj.value}, 'dossier_renamed',obj);
					itm.nom = obj.value;
				}
				else
					obj.classList.remove('editing');
			}

			function dossier_renamed(response,obj)
			{
				if (response.code == 200)
				{
					refresher = document.createElement('iframe');
					refresher.src = 'https://webmail.' + query.get('server') + '/?_task=settings&_action=folders';
					refresher.style.display = 'none';
					document.body.appendChild(refresher);
					document.title = obj.value;
					obj.classList.remove('editing');
				}
			}

			function filename(obj)
			{
				val = obj.value.toString();
				start = obj.selectionStart;
				stp = obj.selectionEnd;
				len = val.length;
				val=val.replace(/\//g,'');
				val=val.replace(/\\/g,'');
				val=val.replace(/\?/g,'');
				val=val.replace(/\</g,'');
				val=val.replace(/\>/g,'');
				val=val.replace(/:/g,'');
				val=val.replace(/\*/g,'');
				val=val.replace(/\|/g,'');
				val=val.replace(/\s+/g,' ');
				obj.value=val;
				obj.setSelectionRange(start+val.length-len,stp+val.length-len)
			}

			var update_link = new BroadcastChannel('update_link');
			update_link.onmessage = function (ev) {window.location.reload()}
			window.onblur = function(){update_link.postMessage('update_link')}
		</script>
	</body>
</html>