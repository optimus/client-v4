<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/editor.css">
		<link rel="stylesheet" href="/css/statictable.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
	</head>
	
	<body style="text-align:center">
	
		<br/>
		
		<table id="intervenants" align="center" class="statictable" style="min-width:350px">
		
		</table>
		
		<br/><br/>
		
		<label for="qualite">Qualité</label><br/>
		<select id="qualite" style="width:350px" onchange="change_qualite(this)"></select><br/>
		<label for="contact">Intervenant</label><br/>
		<select id="contact" style="width:322px"></select>&nbsp;<img style="vertical-align:middle;cursor:pointer;width:24px" src="/modules/contacts/icon.svg" onclick="edit_contact(document.getElementById('contact'))"><br/>
		<label for="lien" id="lien_label" style="visibility:hidden">Lien</label><br/>
		<select id="lien" style="width:350px;visibility:hidden"><option value="0"></option></select><br/>
		<br/>
		<button onclick="intervenant_create()"><img src="/lib/fontawesome/user-plus.svg" style="width:16px;filter:contrast(60%);vertical-align:-2px">&nbsp;&nbsp;AJOUTER</button>
	
		<script type="text/javascript">

			const query = new URLSearchParams(window.location.search);
			api_call(query.get('server'),'allspark/logged','GET',{},'init');
			function init(response)
			{
				contacts = [];
				get_contacts(document.getElementById('contact'));
				qualites = [];
				get_qualites(document.getElementById('qualite'));
				intervenants = [];
				get_intervenants();
			}
			
			function intervenant_create()
			{
				if (document.getElementById('qualite').value != 0 && document.getElementById('contact').value != 0)
					api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/intervenants','POST',{"contact":document.getElementById('contact').value, "qualite":document.getElementById('qualite').value,"lien":document.getElementById('lien').value}, 'intervenant_created');
			}
			
			function intervenant_created(response)
			{
				window.location.reload();
			}
			
			function get_intervenants()
			{
				api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/intervenants','GET',{}, 'display_intervenants');
			}
			
			function display_intervenants(response)
			{
				obj = document.getElementById('intervenants');
				obj.innerHTML = '';
				document.getElementById('lien').innerHTML='<option value="0"></option>';
				intervenants = response.data;
				intervenants.sort(function(x,y){var a = String(x.qualite); var b = String(y.qualite); if (a > b) return 1;if (a < b) return -1;return 0;});
				last_qualite=0;
				for (i=0;i<intervenants.length;i++)
					if (intervenants[i].lien == 0)
					{
						if (intervenants[i].qualite != last_qualite)
						{
							thead = document.createElement('thead');
							thead.innerHTML = '<tr><td colspan="2">'+qualites[intervenants[i].qualite].toUpperCase()+'</td></tr>';
							obj.appendChild(thead);
							last_qualite = intervenants[i].qualite;
						}
							
						row_insert(intervenants[i].id, intervenants[i].qualite, intervenants[i].contact, intervenants[i].lien);
						
						for (j=0;j<intervenants.length;j++)
							if (intervenants[j].lien == intervenants[i].contact)
								row_insert(intervenants[j].id, intervenants[j].qualite, intervenants[j].contact, intervenants[j].lien);

						for (j=0; j<contacts.length; j++)
							if (contacts[j][0] == intervenants[i].contact)
								document.getElementById('lien')[document.getElementById('lien').options.length] = new Option(contacts[j][1],contacts[j][0]);
					}
				
				
			}
			
			function row_insert(id, qualite, contact, lien)
			{
				tr = document.createElement('tr');
				tr.id = id;
				tr.value = contact;
				tr.style.background = '#FFFFFF';
				td1 = document.createElement('td');
				td1.id = contact;
				td1.value = contact;
				td1.style.textAlign = 'left';
				if (lien != 0)
					td1.innerHTML += '&rarr; ';
				for (k=0; k<contacts.length; k++)
					if (contacts[k][0] == contact)
						td1.innerHTML += contacts[k][1];
				if (lien != 0)
					td1.innerHTML += ' (' + qualites[qualite] + ')';
				td1.onclick = function(){edit_contact(this)}
				td1.style.cursor = 'pointer';
				tr.appendChild(td1);
				
				td2 = document.createElement('td');
				td2.id = id;
				td2.style.cursor = 'pointer';
				td2.onclick = function()
				{
					api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/dossiers/'+query.get('id')+'/intervenants/'+this.id,'DELETE',{}, 'get_intervenants');
				}
				img = document.createElement('img');
				img.src = '/lib/fontawesome/trash.svg';
				img.style.width = '14px';
				img.style.filter = 'contrast(60%)';
				tr.appendChild(td2);
				td2.appendChild(img);
				
				document.getElementById('intervenants').appendChild(tr);
			}
			
			
			function get_qualites(obj)
			{
				obj.innerHTML='<option value="">LOADING...</option>';
				results = api_call_sync('optimus-avocats.fr','constants/','GET',{"db":"intervenants_qualites"});
				results.data.sort(function(x,y){var a = String(x.value).toUpperCase(); var b = String(y.value).toUpperCase(); if (a > b) return 1;if (a < b) return -1;return 0;});
				
				obj.innerHTML='<option value="0"></option>';
				if (results.data)
					for (i=0;i<results.data.length;i++)
					{
						qualites[results.data[i].id] = results.data[i].value;
						obj.options[i+1] = new Option(results.data[i].value,results.data[i].id);
					}
			}
			

			function change_qualite(obj)
			{
				if (obj.value == 30 || obj.value == 35 || obj.value == 40)
					get_contacts(document.getElementById('contact'),30);
				else if (obj.value == 50)
					get_contacts(document.getElementById('contact'),50);
				else if (obj.value == 60)
					get_contacts(document.getElementById('contact'),60);
				else if (obj.value == 70)
					get_contacts(document.getElementById('contact'),70);
				else if (obj.value == 110)
					get_contacts(document.getElementById('contact'),110);
				else if (obj.value == 115 || obj.value == 120)
					get_contacts(document.getElementById('contact'),120);
				else
					get_contacts(document.getElementById('contact'));
				
				if (obj.value < 30 || obj.value == 50 || obj.value == 90)
				{
					document.getElementById('lien').value = 0;
					document.getElementById('lien').style.visibility = 'hidden';
					document.getElementById('lien_label').style.visibility = 'hidden';
				}
				else
				{
					document.getElementById('lien').style.visibility = 'visible';
					document.getElementById('lien_label').style.visibility = 'visible';
				}
			}
			
			
			function get_contacts(obj,categorie)
			{
				if (categorie)
					api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/contacts','GET',{"fields":["id","firstname","lastname","company_name"],"filters":[{"categorie":categorie}]}, 'fill_contacts', obj);
				else
					api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/contacts','GET',{"fields":["id","firstname","lastname","company_name"]}, 'fill_contacts', obj);
			}
			
			function fill_contacts(response,obj)
			{
				contacts = [];
				for (i=0;i<response.data.length;i++)
					contacts[i] = [response.data[i].id,(response.data[i].lastname + ' ' + response.data[i].firstname  +' ' + (response.data[i].company_name || '')).trim()];
				
				obj.innerHTML='<option value="">LOADING...</option>';
				obj.innerHTML='<option value="0"></option>';
				contacts.sort(function(x,y){var a = String(x[1]).toUpperCase(); var b = String(y[1]).toUpperCase(); if (a > b) return 1;if (a < b) return -1;return 0;});

				for (i=0;i<contacts.length;i++)
					obj.options[i+1] = new Option(contacts[i][1],contacts[i][0]);
			}
			
			function edit_contact(obj)
			{
				if (obj.value==0) 
				{
					if(confirm('Voulez-vous créer un nouveau contact ?')) 
						api_call(query.get('server'),'optimus-avocats/'+query.get('owner')+'/contacts','POST',{}, 'contact_created', obj);
				} 
				else 
					window.open('/modules/contacts/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+obj.value,'editor');
			}
			
			function contact_created(response,obj)
			{
				obj.add(new Option(response.data.lastname, response.data.id));
				obj.value = response.data.id;
				window.open('/modules/contacts/editor.php?server='+query.get('server')+'&owner='+query.get('owner')+'&id='+response.data.id,'editor2');
			}
		</script>
	</body>
</html>