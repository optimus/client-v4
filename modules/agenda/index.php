<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8' />
	<title>AGENDA</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/images/32x32_intervention.png" />

	<link href="/lib/fullcalendar/core/main.optimus.css" rel="stylesheet" />
	<link href="/lib/fullcalendar/daygrid/main.min.css" rel="stylesheet" />
	<link href="/lib/fullcalendar/timegrid/main.min.css" rel="stylesheet" />
	<link href="/lib/fullcalendar/list/main.min.css" rel="stylesheet" />

	<script src="/lib/fullcalendar/core/main.min.js"></script>
	<script src="/lib/fullcalendar/interaction/main.min.js"></script>
	<script src="/lib/fullcalendar/daygrid/main.min.js"></script>
	<script src="/lib/fullcalendar/timegrid/main.min.js"></script>
	<script src="/lib/fullcalendar/list/main.min.js"></script>

	<link rel="stylesheet" href="/lib/nice-date-picker/nice-date-picker.css">
	<script src="/lib/nice-date-picker/nice-date-picker.js"></script>
</head>

<body style="margin:8px;padding:0;font:normal 14px Roboto;overflow:hidden;text-align: center;">
	
	<table>
		<tr>
			<td style="vertical-align:top;padding-right:12px;text-align:left;font:normal 12px Roboto;line-height:28px">
				<div id="datepicker"></div>
				<input id="colorpicker" type="color" style="display:none" onchange="current.style.borderColor=this.value"/>
				
				<div id="agendas">
				
					<div id="associe2" style="border:1px solid #808080;background:#5E79B0;color:#FFFFFF;text-align:center;margin-top:8px"><b>LIONEL VEST</b>
						<div id="agenda1" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if(event.target.nodeName!='SPAN') if (this.firstChild.style.backgroundColor=='transparent') {this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; show_calendar(2);} else {this.firstChild.style.backgroundColor='transparent';hide_calendar(2)}" style="cursor:pointer"><span id="color1" style="width:16px;border:2px solid #FF0000;background-color:#FF0000" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Audiences</div>
						<div id="agenda2" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if (this.firstChild.style.backgroundColor=='transparent') this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; else this.firstChild.style.backgroundColor='transparent'" style="cursor:pointer"><span id="color2" style="width:16px;border:2px solid #008000;background-color:transparent" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Reproductions</div>
						<div id="agenda3" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if (this.firstChild.style.backgroundColor=='transparent') this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; else this.firstChild.style.backgroundColor='transparent'" style="cursor:pointer"><span id="color3" style="width:16px;border:2px solid #0000AA;background-color:transparent" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Rendez-vous</div>
					</div>
					
					<div id="associe2" style="border:1px solid #808080;background:#5E79B0;color:#FFFFFF;text-align:center;margin-top:8px"><b>CHLOE BRILL</b>
						<div id="agenda1" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if(event.target.nodeName!='SPAN') if (this.firstChild.style.backgroundColor=='transparent') {this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; show_calendar(2);} else {this.firstChild.style.backgroundColor='transparent';hide_calendar(2)}" style="cursor:pointer"><span id="color1" style="width:16px;border:2px solid #FF0000;background-color:#FF0000" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Audiences</div>
						<div id="agenda2" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if (this.firstChild.style.backgroundColor=='transparent') this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; else this.firstChild.style.backgroundColor='transparent'" style="cursor:pointer"><span id="color2" style="width:16px;border:2px solid #008000;background-color:transparent" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Reproductions</div>
						<div id="agenda3" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if (this.firstChild.style.backgroundColor=='transparent') this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; else this.firstChild.style.backgroundColor='transparent'" style="cursor:pointer"><span id="color3" style="width:16px;border:2px solid #0000AA;background-color:transparent" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Rendez-vous</div>
					</div>
					
					<div id="associe2" style="border:1px solid #808080;background:#5E79B0;color:#FFFFFF;text-align:center;margin-top:8px"><b>ANTOINE BON</b>
						<div id="agenda1" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if(event.target.nodeName!='SPAN') if (this.firstChild.style.backgroundColor=='transparent') {this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; show_calendar(2);} else {this.firstChild.style.backgroundColor='transparent';hide_calendar(2)}" style="cursor:pointer"><span id="color1" style="width:16px;border:2px solid #FF0000;background-color:#FF0000" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Audiences</div>
						<div id="agenda2" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if (this.firstChild.style.backgroundColor=='transparent') this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; else this.firstChild.style.backgroundColor='transparent'" style="cursor:pointer"><span id="color2" style="width:16px;border:2px solid #008000;background-color:transparent" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Reproductions</div>
						<div id="agenda3" style="background:#EBF1FF;text-align:left;color:#000000;padding:2px" onclick="if (this.firstChild.style.backgroundColor=='transparent') this.firstChild.style.backgroundColor=this.firstChild.style.borderColor; else this.firstChild.style.backgroundColor='transparent'" style="cursor:pointer"><span id="color3" style="width:16px;border:2px solid #0000AA;background-color:transparent" onclick="current=this;colorpicker.click()">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Rendez-vous</div>
					</div>
				</div>
			</td>
			
			<td>
				<div id="fullcalendar" style="background:#FFFFFF"></div>
			</td>
			
		</tr>
	</table>
	
</body>


<script>
	
	alert('Attention : cette version de démonstration n\'est pas encore fonctionnelle !')
	
	if (window.innerWidth < 800)
	{
		document.getElementById('datepicker').style.display = 'none';
		document.getElementById('agendas').style.display = 'none';
	}
	
	calendar = new FullCalendar.Calendar(document.getElementById('fullcalendar'), 
    {
		plugins: ['interaction','dayGrid','timeGrid','list'],
		header: 
		{
			left: 'prev,next today',
			center: 'title',
			right: 'timeGrid,workweek,timeGridWeek,dayGridMonth,list'
		},
		buttonText: 
		{
			timeGrid: 'jour',
			today: 'aujourd\'hui',
			workweek: '5j',
			timeGridWeek: '7j',
			dayGridMonth: 'mois',
			list: 'liste'
		},
		views: 
		{
			workweek: 
			{
				type: 'timeGrid',
				duration: {weeks: 1},
				weekends: false,
			}
		},
		defaultView: 'workweek',
		locale: 'fr',
		contentHeight: window.innerHeight - 80,
		scrollTime: '06:00:00',
		firstDay: 1,
		nowIndicator: true,
		navLinks: true, // can click day/week names to navigate views
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		eventLongPressDelay: 500,
		selectLongPressDelay: 500,
		eventSources: 
		[
	      {
		      id: 2,
		      backgroundColor: '#FF0000',
		      events:
		      [
		        {
		          title: 'AGRILINE / JARDINERIE JAY',
		          url: 'http://google.com/',
		          start: '2019-12-09T10:30:00Z',
		          end: '2019-12-09T12:00:00Z',
		          color: '#FF0000',
		        },
				{
		          title: 'AGRILINE / TARTAMPION',
		          url: 'http://google.com/',
		          start: '2019-12-10T14:30:00Z',
		          end: '2019-12-10T16:00:00Z',
		          color: '#FF0000',
		        },
		        {
		          title: 'AGRILINE / TRUCMUCHE',
		          url: 'http://google.com/',
		          start: '2019-12-11T10:30:00Z',
		          end: '2019-12-11T14:00:00Z',
		          color: '#FF0000'
		        },
				{
		          title: 'AGRILINE / RJ',
		          url: 'http://google.com/',
		          start: '2019-12-11T08:30:00Z',
		          end: '2019-12-11T10:00:00Z',
		          color: '#FF0000'
		        },
		      ]
	      },
	      {
		      id: 5,
		      backgroundColor: '#008000',
		      events:
		      [
		        {
		          title: 'Evènement long',
		          start: '2019-12-08',
		          end: '2019-12-10',
		        },
		        {
		          groupId: 999,
		          title: 'Evènement récurrent',
		          start: '2019-12-07T16:00:00Z'
		        },
		        {
		          groupId: 999,
		          title: 'Repeating Event',
		          start: '2019-12-08T16:00:00Z'
		        },
		        {
		          groupId: 999,
		          title: 'Repeating Event',
		          start: '2019-12-09T16:00:00Z'
		        },
		      ]
	      },
			],
			eventClick: function(info) 
			{
		    alert('Event: ' + info.event.title);
		    alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
		    alert('View: ' + info.view.type);
		    info.el.style.borderColor = 'red';
		  }
      
    });

		calendar.render();
	
		new niceDatePicker(
		{
			dom:document.getElementById('datepicker'),
			onClickDate:function(date)
			{
				date = new Date(date);
				calendar.gotoDate(date);
			}
    });
    
    function hide_calendar(id)
    {
    	calendar.getEventSourceById(id).remove();
    }
    
    function show_calendar(id)
    {
    	calendar.getEventSourceById(id).refetch();
    }
    
    function toggle_calendar(id)
    {
    	
    }
    
    function update_event(id)
    {
    	
    }
    
    function delete_event(id)
    {
    	
    }
    
    function add_event(id)
    {
    	
    }
</script>
</html>
