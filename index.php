<html lang="fr">
<head>
	<title>OPTIMUS*</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=yes">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/logo/favicon_16x16.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/logo/favicon_32x32.png">
	<link rel="icon" type="image/png" sizes="180x180" href="/images/logo/favicon_180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/images/logo/favicon_192x192.png">
	<link rel="icon" type="image/png" sizes="512x512" href="/images/logo/favicon_512x512.png">
	<link href="/css/main.css" rel="stylesheet" type="text/css">
	<link href="/css/homepage.css" rel="stylesheet" type="text/css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/cookies.js"></script>
</head>
<body>
	<img src="/images/logo/optimus-avocats.svg" style="width:calc(100% - 30px);max-width:300px;margin-top:60px" />
	<img src="/images/logout.svg" style="position:absolute;top:5px;right:5px;width:48px;height:48px;filter:drop-shadow(2px 2px 4px rgba(0,0,0,0.5))" onclick="logout()" />
	<div id="server" style="position:absolute;top:10px;left:10px;height:16px;display:flex;align-items: center;"></div>
	<div id="user" style="position:absolute;top:34px;left:10px;height:16px;font:bold 12px Roboto;border:0px;color:#000000;text-align:left;display:flex;align-items: center;"></div>
	<div id="container"></div>
	OPTIMUS V<span id="client_version"></span> <a href="https://www.cybertron.fr" target="_blank" style="color:#5E79B0;text-decoration:none" >by Cybertron</a><br/>
	<span id="browser_compatibility" style="color:#FF0000;display:none">Ce logiciel est conçu pour être utilisé avec Chrome ou Firefox. Les autres navigateurs n'ont pas été testés de manière poussée.</span>
</body>

<script>
	client_version = window.location.hostname.split('.')[0];
	document.getElementById('client_version').innerHTML = client_version.slice(1,2) + '.' + client_version.slice(2);
	document.title = 'OPTIMUS V' + client_version.slice(1,2) + '.' + client_version.slice(2);

	if (localStorage.getItem('version') != client_version)
	{
		localStorage.clear();
		localStorage.setItem('version',client_version);
	}

	if (detect_browser() != 'Chrome' && detect_browser() != 'Firefox')
		document.getElementById('browser_compatibility').style.display = '';

	const query = new URLSearchParams(window.location.search);
	if (query.get('server') && query.get('server') != get_cookie('server'))
		login_open();

	if (get_cookie('server') && get_cookie('user_id'))
		api_call(get_cookie('server'),'allspark/logged','GET',{},'init');
	else
		login_open();
	
	
	function init(response)
	{	
		api_call(get_cookie('server'),'allspark/service','GET',{},'check_updates');
		api_call(get_cookie('server'),'optimus-avocats/service','GET',{},'check_updates');
		api_call(get_cookie('server'),'optimus-structures/service','GET',{},'check_updates');

		document.getElementById('server').innerHTML = '<img src="/lib/fontawesome/server.svg" style="filter:contrast(40%);width:14px"/>&nbsp;&nbsp;<input value="' + get_cookie('server') +'" onchange="api_call(this.value,\'allspark/logged\',\'GET\',{},\'init\')"/>';
		users = api_call_sync(get_cookie('server'),'allspark/users/'+response.data.id,'GET',{});
		services = api_call_sync(get_cookie('server'),'allspark/users/'+get_cookie('user_id')+'/services','GET',{});
		document.getElementById('user').innerHTML = '<img src="/lib/fontawesome/user.svg" style="filter:contrast(40%);width:14px;margin-right:12px"/>' + (users.data.firstname || '') + ' ' + (users.data.lastname || '') +'<br/>';
		
		document.getElementById('container').innerHTML='';

		modules = api_call_sync(get_cookie('server'),'allspark/modules','GET',{});

		services.data.push({name:'general',version:'1.0',version_date:'2021-10-10'})
		services.data.push({name:'tools',version:'1.0',version_date:'2021-10-10'})
		services.data.push({name:'administration',version:'1.0',version_date:'2021-10-10'})

		structures = api_call_sync(get_cookie('server'),'optimus-avocats/'+get_cookie('user_id')+'/structures','GET',{});
		if (structures?.data?.length > 0)
			services.data.push({name:'optimus-structures',version:'1.0',version_date:'2021-10-10'})

		for (let i=0; i < modules.data.groups.length ; i++)
		if (services.data.find(service => service.name == modules.data.groups[i].name))
		{
			group = document.createElement('div');
			group.id = modules.data.groups[i].name;
			group.className = 'icon_group';
			group.style.display = 'block';
			group.style.width = 'calc(100% - 32px)';
			group.style.maxWidth = (modules.data.groups[i].modules.length*96+32) + 'px';
			group.style.margin = '32px auto';
			document.getElementById('container').appendChild(group);

			titlebar = document.createElement('div');
			titlebar.innerHTML = modules.data.groups[i].displayname.toUpperCase();
			group.appendChild(titlebar);

			module_container = document.createElement('div');
			group.appendChild(module_container);

			for (let j=0; j < modules.data.groups[i].modules.length ; j++)
			{
				module =  document.createElement('span');
				module.id = modules.data.groups[i].modules[j].name;
				if (modules.data.groups[i].modules[j].admin_only <= get_cookie('user_admin'))
					module.onclick = function(){window.open('/modules/' + modules.data.groups[i].modules[j].name + '/index.php?server='+document.getElementById('server').children[1].value+'&owner='+get_cookie('user_id'), modules.data.groups[i].modules[j].name).focus()};
				else
					module.onclick = function(){alert('Cette fonction est réservée aux administrateurs')}
				module_container.appendChild(module);

				module_image = document.createElement('img');
				module_image.src = '/modules/' + modules.data.groups[i].modules[j].name + '/icon.svg';
				module.appendChild(module_image);

				if (modules.data.groups[i].modules[j].admin_only > get_cookie('user_admin'))
				{
					module_image = document.createElement('img');
					module_image.src = '/images/forbidden.svg';
					module_image.style.marginLeft = '-24px';
					module_image.style.width = '24px';
					module_image.style.height = '24px';
					module.appendChild(module_image);
				}

				module_text = document.createElement('span');
				module_text.style.display = 'block';
				module_text.innerHTML = modules.data.groups[i].modules[j].displayname;
				module.appendChild(module_text);

				if (modules.data.groups[i].modules[j].disabled == true)
				{
					module_text.innerHTML = modules.data.groups[i].modules[j].displayname;
					module.style.opacity = '0.3';
					module.style.filter = 'grayscale(1)';
					module.onclick = function(){alert('Cette fonction sera activée dans les prochains jours')};
				}
			}
		}
	}
	
	function logout()
	{
		delete_cookie('user_id', '.' + window.location.hostname.split('.').slice(1).join('.'));
		delete_cookie('user_admin', '.' + window.location.hostname.split('.').slice(1).join('.'));
		//delete_cookie('roundcube_sessid', 'webmail.demoptimus.fr');
		//delete_cookie('roundcube_sessauth', 'webmail.demoptimus.fr');
		api_call(get_cookie('server'),'allspark/logout','GET',{}, window.location.href = 'https://optimus.cybertron.fr');
		
	}
	
	window.onmessage = function (event) {if (event.data == 'logged') window.location.reload()}

	
	function check_updates(response)
	{
		if (response.code == 200)
			if (response.data.latest_date > response.data.current_date)
				if (!document.getElementById('update_badge'))
				{
					badge = document.createElement('p');
					badge.id = 'update_badge';
					badge.innerHTML = 1;
					document.getElementById('server-admin').appendChild(badge);
				}
				else
					document.getElementById('update_badge').innerHTML = parseInt(document.getElementById('update_badge').innerHTML) + 1;
	}
</script>
</html>
