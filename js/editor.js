function editor_init() {
	inputs = document.querySelectorAll('input,select,textarea');
	for (i = 0; i < inputs.length; i++)
		if (typeof itm[inputs[i].id] !== 'undefined') {
			if (inputs[i].type == 'checkbox' && itm[inputs[i].id] == 1)
				inputs[i].checked = true;
			else
				inputs[i].value = itm[inputs[i].id];

			if (authorizations.write == 1 && inputs[i].getAttribute('readonly') == null) {
				if (!inputs[i].onkeydown && inputs[i].type != 'textarea')
					inputs[i].onkeydown = function () { if (event && event.keyCode == 13) this.blur() }

				if (!inputs[i].onfocus)
					inputs[i].onfocus = function () { this.classList.add('editing') }

				if (inputs[i].type == 'date' && !inputs[i].onclick)
					inputs[i].onclick = function () { if (document.activeElement != this) this.focus() }

				if (inputs[i].type == 'date' && !inputs[i].oncontextmenu)
					inputs[i].oncontextmenu = function () { this.value = '' }

				if (!inputs[i].oncontextmenu && inputs[i].type != 'checkbox' && inputs[i].type != 'button' && inputs[i].type != 'select')
					inputs[i].oncontextmenu = function () { return false; }
			}
			else if (inputs[i].getAttribute('readonly') == null)
				inputs[i].disabled = true;
		}
}

function updated(response, obj) {
	if (response.code >= 200 && response.code <= 299 && (obj != document.activeElement || obj.type == 'select-one')) {
		obj.classList.remove('editing');
		obj.blur();
	}
	else {
		obj.classList.add('editing');
		obj.focus();
	}

}

function simple_save(obj) {
	if (obj.type == 'checkbox' && obj.checked)
		value = 1;
	else if (obj.type == 'checkbox' && !obj.checked)
		value = 0;
	else
		value = obj.value;

	if (itm[obj.id] != value) {
		api_call(query.get('server'), api + '/' + query.get('owner') + '/' + resource + '/' + query.get('id'), 'PATCH', { [obj.id]: value }, 'updated', obj);
		itm[obj.id] = value;
	}
	else
		obj.classList.remove('editing');
}

function populate(obj, constants, order, addzero, setvalue, filter) {
	if (localStorage.getItem(constants))
		results = JSON.parse(localStorage.getItem(constants));
	else {
		results = api_call_sync('optimus-avocats.fr', 'constants/', 'GET', { "db": constants });
		if (results.code == 200)
			localStorage.setItem(constants, JSON.stringify(results));
	}
	obj.innerHTML = '';
	if (addzero == true)
		obj.innerHTML = '<option value="0"></option>';
	if (order == true)
		results.data.sort(function (x, y) { var a = String(x.value).toUpperCase(); var b = String(y.value).toUpperCase(); if (a > b) return 1; if (a < b) return -1; return 0; });
	for (i = 0; i < results.data.length; i++)
		if (!filter || (filter && results.data[i][Object.keys(results.data[i])[1]] == filter))
			obj.options[obj.options.length] = new Option(results.data[i].value, results.data[i].id);
	if (setvalue)
		obj.value = setvalue;
}